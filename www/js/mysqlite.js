var app = angular.module('app.mysqlite', []);

app.factory('$mysqlite', function ($cordovaSQLite, CommonService, $schema) {
  var db;
  var service = {};


  service.open = function () {
    //db = $cordovaSQLite.openDB({ name: $schema.DATEBASE_NAME });
    db = window.sqlitePlugin.openDatabase({ name: $schema.DATEBASE_NAME, location: 1});
  };
  /**
   * insert
   * @param table 表名
   * @param params json对象{}（键--数据库字段）
   * @returns {"rows":{"length":0},"rowsAffected":1,"insertId":1}
   */
  service.insert = function (table, params) {
    /*if (typeof table == 'undefined') {
      throw new Error('表不存在');
    }
    if (typeof params == 'undefined') {
      throw new Error('数据不存在');
    }*/

    var arr = [];
    var keys = [];
    var values = [];
    for (var key in params) {
      keys.push(key);
      values.push("?");
      arr.push(params[key]);
    }
    var sql = "INSERT INTO " + table + " (" + keys.join(',') + ") values (" + values.join(',') + ")";

    return new Promise(function (resolve, reject) {
      $cordovaSQLite.execute(db, sql, arr).then(function (rows) {
        resolve(rows.insertId);
      }, function (e) {
        reject(e);
      })
    });
  };

  /**
   * delete
   * @param table 表名
   * @param where
   * @returns {*}
   */
  service.delete = function (table, where) {
    /*if (typeof table == 'undefined') {
      throw new Error('表不存在');
    }*/

    where = where || '';
    var sql = "DELETE FROM " + table + " " + where;

    return new Promise(function (resolve, reject) {
      $cordovaSQLite.execute(db, sql).then(function (rows) {
        resolve();
      }, function (e) {
        reject(e);
      })
    });
  };

  /**
   * update
   * @param table 表名
   * @param params json对象{}（键--数据库字段）
   * @param where
   * @returns {"rows":{"length":0},"rowsAffected":1,"insertId":1}
   */
  service.update = function (table, params, where) {
    /*if (typeof table == 'undefined') {
      throw new Error('表不存在');
    }
    if (typeof params == 'undefined') {
      throw new Error('数据不存在');
    }*/

    where = where || '';
    var sets = [];
    for (var key in params) {
      if (params[key])
        sets.push(key + "='" + params[key] + "'");
    }
    var sql = "UPDATE " + table + " SET " + sets.join(',') + " " + where;
    return new Promise(function (resolve, reject) {
      $cordovaSQLite.execute(db, sql).then(function (rows) {
        resolve(rows);
      }, function (e) {
        reject(e);
      })
    });
  };

  /**
   * select
   * @param query
   * @returns {count:3,data:[]}
   */
  service.select = function (query) {
    return new Promise(function (resolve, reject) {
      $cordovaSQLite.execute(db, query).then(function (rows) {
        if(rows.rows.length > 0){
          var res = [];
          for(var i=0;i<rows.rows.length;i++){
            var row = rows.rows.item(i);
            res.push(row);
          }
          resolve({count: rows.rows.length,data: res});
        }else
          resolve({count: 0,data: []});
      }, function (e) {
        reject(e);
      })
    });
  };
  /**
   * select
   * @param query
   * @returns {*}
   */
  service.selectOne = function (query) {
    return new Promise(function (resolve, reject) {
      $cordovaSQLite.execute(db, query).then(function (rows) {
        if(rows.rows.length > 0){
          resolve(rows.rows.item(0));
        }else
          resolve({});
      }, function (e) {
        reject(e);
      })
    });
  };

  /**
   * create drop
   * @param query
   * @returns {*}
   */
  service.execute = function (query) {
    return new Promise(function (resolve, reject) {
      $cordovaSQLite.execute(db, query).then(function (rows) {
        resolve(rows);
      }, function (e) {
        reject(e);
      })
    });
  };

  /**新建数据库表 */
  var upgradeDb = function () {
    return new Promise.resolve().then(function () {
      return [$schema.TABLE_NAME_CONFIG, $schema.TABLE_NAME_SERVICE, $schema.TABLE_NAME_FAIL_SERVICE,
        $schema.TABLE_NAME_ITEM, $schema.TABLE_NAME_METADATA, $schema.TABLE_NAME_SERVICEFEE, $schema.TABLE_NAME_BRAND_CATEGORY,
        $schema.TABLE_NAME_DEPARTMENT, $schema.TABLE_NAME_UPDATE_TIME, $schema.TABLE_NAME_SETTING];
    }).each(function (t) {
      return service.execute("drop table if exists " + t);
    }).then(function () {
      return [$schema.T_STRUCTURE_CONFIG, $schema.T_STRUCTURE_SERVICE, $schema.T_STRUCTURE_FAIL_SERVICE,
        $schema.T_STRUCTURE_ITEM, $schema.T_STRUCTURE_METADATA, $schema.T_STRUCTURE_SERVICEFEE, $schema.T_STRUCTURE_BRAND_CATEGORY,
        $schema.T_STRUCTURE_DEPARTMENT, $schema.T_STRUCTURE_UPDATE_TIME, $schema.T_STRUCTURE_SETTING];
    }).each(function (t_structure) {
      var create_sql = service.tableCreateSql(t_structure);
      return service.execute(create_sql);
    }).then(function () {
      var params = {
        ip: CommonService.SERVER_IP, port: CommonService.SERVER_PORT, wcode: CommonService.LOGIN_ACCOUNT, login_account: CommonService.LOGIN_ACCOUNT,
        password: CommonService.PASSWORD, version: $schema.DATEBASE_VERSION, tenant_id: CommonService.TENANT_ID
      };
      return service.insert($schema.TABLE_NAME_CONFIG, params);
    }).then(function () {
      return $schema.DATEBASE_VERSION;
    });
  };

  /**获取基本配置和数据库版本 */
  var getDbData = function () {
    var ver;
    return service.selectOne('SELECT count(*) count FROM sqlite_master WHERE type="table" AND name = "' + $schema.TABLE_NAME_CONFIG + '"').then(function (row) {
      if (row && row.count > 0) {//判断config表是否存在  存在
        return service.selectOne("select * from " + $schema.TABLE_NAME_CONFIG).then(function (res) {
          CommonService.LOGIN_ACCOUNT = res.login_account;
          CommonService.PASSWORD = res.password;
          CommonService.TENANT_ID = res.tenant_id;
          CommonService.JPUSH_TAGS = res.tags;
          CommonService.JPUSH_ALIAS = res.alias;
          CommonService.MBT_NAME = res.mBtName;
          CommonService.MBT_ADDRESS = res.mBtAddress;
          ver = res.version;
        });
      } else {//不存在 新建所有表
        return upgradeDb().then(function (r) {
          ver = r;
        });
      }
    }).then(function () {
      return ver;
    });
  };

  service.initConfig = function () {
    if(!db)
      service.open();
    return getDbData().then(function (version) {
      //数据库版本比较
      if (version < $schema.DATEBASE_VERSION) {
        //旧app数据库版本低，清数据库 重新创建表结构 重新加载数据
        return upgradeDb();
      }
    });
  };

  /**
   * 服务端获取对应数据，先清空手机端对应数据，然后将列表依次插入到手机端的对应表中
   * @param tableName
   * @param tableStructure
   * @param list
   * @param type：S工单、B基础数据、I配件、G产品
   * @param context：metadata表中对应字段context的值，操作metadata表时 此参数传对应值
   * @returns {*}
   */
  service.refreshDb = function (tableName, tableStructure, list, type, context) {
    if (list == '') {//从服务端拉取的数据为空，手机端只清空对应表
      return new Promise.resolve().then(function () {
        if (context)
          return service.delete(tableName, "where context = '" + context + "'");
        else
          return service.delete(tableName);
      }).then(function () {
        return service.delete($schema.TABLE_NAME_UPDATE_TIME, "where name='" + type + "'");
      }).then(function () {
        return service.insert($schema.TABLE_NAME_UPDATE_TIME, { name: type, value: moment().format('YYYY-MM-DD HH:mm:ss') });
      }).then(function () {
        return true;
      });
    } else {//从服务端拉取有数据，手机端清空对应表并重新赋值
      return new Promise.resolve().then(function () {
        if (context)
          return service.delete(tableName, "where context = '" + context + "'");
        else
          return service.delete(tableName);
      }).then(function () {
        return list;
      }).each(function (obj) {
        var params = service.tableToParams(tableStructure, obj);
        return service.insert(tableName, params);
      }).then(function () {
        return service.delete($schema.TABLE_NAME_UPDATE_TIME, "where name='" + type + "'");
      }).then(function () {
        return service.insert($schema.TABLE_NAME_UPDATE_TIME, { name: type, value: moment().format('YYYY-MM-DD HH:mm:ss') });
      }).then(function () {
        return true;
      });
    }
  };

  service.updateDbItems = function (tableName, tableStructure, list, count, total) {
    if (count <= 0) {
      return new Promise.resolve().then(function () {
          return service.delete(tableName);
      }).then(function () {
        return service.delete($schema.TABLE_NAME_UPDATE_TIME, "where name='I'");
      }).then(function () {
        return service.insert($schema.TABLE_NAME_UPDATE_TIME, { name: 'I', value: moment().format('YYYY-MM-DD HH:mm:ss') });
      }).then(function () {
        return true;
      });
    } else {
      return new Promise.resolve().then(function () {
        if(total == 1)
            return service.delete(tableName);
      }).then(function () {
        return list;
      }).each(function (obj) {
        var params = service.tableToParams(tableStructure, obj);
        return service.insert(tableName, params);
      }).then(function () {
        return service.delete($schema.TABLE_NAME_UPDATE_TIME, "where name='I'");
      }).then(function () {
        return service.insert($schema.TABLE_NAME_UPDATE_TIME, { name: 'I', value: moment().format('YYYY-MM-DD HH:mm:ss') });
      }).then(function () {
        return true;
      });
    }
  };


  /**
   * 将schema.js中表结构整理成键值对象
   */
  service.tableToParams = function (t_structure, obj) {
    var params = t_structure.schema;//json对象{}
    for (var key in params) {
      if (obj[key]){
        params[key] = angular.isObject(obj[key])?JSON.stringify(obj[key]):obj[key];
      }else
        params[key] = '';
    }
    return params;
  };

  /**
   * 将schema.js中表结构整理成建表语句
   */
  service.tableCreateSql = function (t_structure) {
    var sets = [];
    var tableName = t_structure.model;
    var schema = t_structure.schema;//json对象{}
    for (var key in schema) {
      sets.push(key + " " + schema[key]);
    }
    var sql = "create table " + tableName + "(" + sets.join(',') + ")";
    return sql;
  };

  return service;
});
