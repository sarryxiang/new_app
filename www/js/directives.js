var app = angular.module('app.directives', []);

app.directive('blankDirective', [function () {

}]);

app.directive('mydatepicker', function () {
    return {
        restrict: 'AE',
        scope: {
            ngModel: '=',
            before: '@',//true仅当天之前的日期(包括当天)可选；默认false（即不限制可选日期）
            after: '@'////true仅当天之后的日期（包括当天）可选；默认false（即不限制可选日期）
        },
        link: function (scope, element) {
            $(element).click(function () {
                scope.click();
            })
        },
        controller: function ($scope, ionicDatePicker) {
            var ipObj1 = {
                callback: function (val) {  //Mandatory
                    $scope.ngModel = moment(val).format('YYYY-MM-DD');
                }
            };
            if ($scope.before)
                ipObj1.to = new Date();
            if ($scope.after)
                ipObj1.from = new Date();
            $scope.click = function () {
                ionicDatePicker.openDatePicker(ipObj1);
            };
        }
    }
});

app.directive('mytimepicker', function () {
    return {
        restrict: 'AE',
        scope: {
            ngModel: '='
        },
        link: function (scope, element) {
            $(element).click(function () {
                scope.click();
            })
        },
        controller: function ($scope, ionicTimePicker) {
            var ipOb2 = {
                callback: function (val) {
                    if (typeof (val) === 'undefined') {
                        console.log('Time not selected');
                    } else {
                        var selectedTime = new Date(val * 1000);
                        $scope.ngModel = selectedTime.getUTCHours() + ':' + 
                            (selectedTime.getUTCMinutes()>9?selectedTime.getUTCMinutes():'0'+selectedTime.getUTCMinutes());
                    }
                }
            };
            $scope.click = function () {
                ionicTimePicker.openTimePicker(ipOb2);
            };
        }
    }
});

app.directive('transState', function () {
    return {
        restrict: 'AE',
        scope: {
            transState: '@'
        },
        template: "{{name}}",
        controller: function ($scope, CommonService) {
            if (!_.isUndefined($scope.transState)) {
                angular.forEach(CommonService.STATE_LIST, function(eml){
                    if(eml.key == $scope.transState)
                        $scope.name = eml.value;
                });
            }
        }
    }
});

//选择弹出层（可联想筛选下拉列表）----请看参数详细说明
app.directive('inputSelect', function(){
    return {
        restrict: 'AE',
        scope: {
            optionName: '@',//下拉选择搜索框默认的文字（如：请选择品牌、请选择品类等）
            selectList: '=',//下拉列表集合--格式:[{key:'',value:''},{key:'',value:''}]
            ngKey: '=',//保存到数据库的键，即字段的值
            ngModel: '=',//显示在页面的文字
            callbackOption: '=',//选择确认后的回调函数
            callbackType: '@'//回调函数类型（值为'null'时，代表没有回调函数；有值并为其他值，为回调函数用的一个区别参数，像品牌品类选择的回调函数就需要
        },
        link: function (scope, element) {
            $(element).click(function () {
                scope.click();
            })
        },
        controller: function ($scope, $ionicModal, $filter) {
            $scope.$watch('ngKey',function(){
                if($scope.ngKey && $scope.ngKey != ''){
                    var a = $filter('filter')($scope.selectList, {key: $scope.ngKey});
                    if(a && a.length > 0){
                        $scope.ngModel = angular.copy(a[0].value);
                    }else{
                        $scope.ngModel = undefined;//'请选择';
                    }
                }else{
                    $scope.ngModel = undefined;//'请选择';
                }
            });

            $scope.click = function(){
                $scope.currentSelect = {text_input: ''};
                $ionicModal.fromTemplateUrl('views/blocks/input_select_modal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.select_modal = modal;
                    $scope.select_modal.show();

                    $scope.$watch('currentSelect.text_input', function () {
                        if ($scope.currentSelect.text_input && $scope.currentSelect.text_input != '') {//查询
                            $scope.curSelList = $filter('filter')($scope.selectList, $scope.currentSelect.text_input);
                        } else
                        $scope.curSelList = $scope.selectList;
                    });
                });
            };

            $scope.selectedClick = function(obj){
                if(obj && obj != '')
                    $scope.ngKey = angular.copy(obj.key);
                else
                    $scope.ngKey = '';
                $scope.currentSelect = {text_input: ''};
                if($scope.callbackType && $scope.callbackType == 'null')
                    $scope.select_modal.hide();
                else {
                    if($scope.callbackType)
                        $scope.callbackOption(obj, $scope.callbackType);
                    else
                        $scope.callbackOption(obj);
                    $scope.select_modal.hide();
                }
            };

            $scope.closeModal = function () {
                $scope.select_modal.hide();
            };     

        }
    }
});

//配件选择加载页面
app.directive('itemSelect', function(){
    return {
        restrict: 'AE',
        scope: {
            item: '='//{ selected:[], cur_select:{} }
        },
        templateUrl: "views/service/items_select.html",
        controller: 'ItemSelectCtrl'
    }
});
//配件选择弹出层
app.directive('itemselModal', function(){
    return {
        restrict: 'AE',
        scope: {
            itemModal: '@',//{ selected:[], cur_select:{} }
            callbackOption: '='//选择确认后的回调函数
        },
        link: function (scope, element) {
            $(element).click(function () {
                scope.showModal();
            })
        },
        controller: 'ItemSelectCtrl'
    }
});
//图片选择 加载页面/弹出层
app.directive('imageSelect', function(){
    return {
        restrict: 'AE',
        scope: {
            oldImages: '=',
            newImages: '=',
            imageModal: '@',//是否是弹出层
            callbackOption: '='//弹出层的回调函数

        },
        templateUrl: "views/service/images_select.html"
    }
});
//工单修改页面“完成”按钮弹出层
app.directive('completeModal', function(){
    return {
        restrict: 'AE',
        scope: {
            ngObj: '@',//
            callbackOption: '='//选择确认后的回调函数
        },
        link: function (scope, element) {
            $(element).click(function () {
                scope.click();
            })
        },
        controller: function ($scope, $ionicModal, $filter, $scancode) {

            $scope.click = function(){
                $ionicModal.fromTemplateUrl('views/service/complete_modal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.select_modal = modal;
                    $scope.service = JSON.parse($scope.ngObj);
                    $scope.select_modal.show();
                });
            };
            //确定
            $scope.ok = function(){
                $scope.select_modal.hide();
                $scope.callbackOption($scope.service);
            };
            //取消
            $scope.closeModal = function () {
                $scope.select_modal.hide();
            };
            //扫描:出厂编码、扩展字段（室外机条码） type类型(e扩展字段)、index(type为扩展字段是，index为扩展字段序号)
            $scope.scan = function (type, index) {
                $scancode.scan().then(function (qrcode) {
                if (type == 'qr')
                    $scope.service.qr_code = qrcode.text;
                else if (type == 'e') {//扫描值赋给扩展字段
                    if (index == 1)
                    $scope.service.extend1 = qrcode.text;
                    else if (index == 2)
                    $scope.service.extend2 = qrcode.text;
                    else if (index == 3)
                    $scope.service.extend3 = qrcode.text;
                    else if (index == 4)
                    $scope.service.extend4 = qrcode.text;
                    else if (index == 5)
                    $scope.service.extend5 = qrcode.text;
                }

                });
            }     

        }
    }
});