
angular.module('app',
  ['ionic',
    'ionic-datepicker',
    'ionic-timepicker',
    'ngCordova',
    'ionicLazyLoad',
    'app.controllers',
    'app.routes',
    'app.directives',
    'app.filters',
    'app.services',
    'app.constants',
    'app.mysqlite',
    'app.schema'
  ]).config(function (ionicDatePickerProvider, ionicTimePickerProvider) {
    var datePickerObj = {
      inputDate: new Date(),
      titleLabel: '选择日期',
      setLabel: '确定',
      //todayLabel: '今天',
      closeLabel: '取消',
      mondayFirst: false,
      weeksList: ["日", "一", "二", "三", "四", "五", "六"],
      monthsList: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
      templateType: 'popup',//popup modal
      from: new Date(2012, 8, 1),
      to: new Date(2018, 8, 1),
      //showTodayButton: true,
      dateFormat: 'dd MMMM yyyy',
      //closeOnSelect: true,
      disableWeekdays: []
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);

    var timePickerObj = {
      inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
      format: 24,//12 or 24
      step: 10,//10/15/20/30
      setLabel: '确定',
      closeLabel: '取消'
    };
    ionicTimePickerProvider.configTimePicker(timePickerObj);


  })
  .run(function ($rootScope, $ionicPlatform, $ionicPopup, $cordovaNetwork,
    $ionicLoading, $cordovaFileTransfer, $cordovaFile, $cordovaFileOpener2, $location, $ionicHistory,
    $cordovaToast, $cordovaKeyboard, $cordovaDevice, $window,
    $timeout, jpushService, $schema, $mysqlite, $myhttp, CommonService, UserService) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      $mysqlite.open();

      /**检测更新 */
      if ($cordovaNetwork.isOnline())
        checkUpdate();

      /** 推送初始化---start*/
      var setTagsWithAliasCallback = function (event) {
        //window.alert('setTagsWithAliasCallback result code:' + event.resultCode + ' tags:' + event.tags + ' alias:' + event.alias);

        if (event && event.resultCode == 6002) {//遇到 6002 超时，则稍延迟重试。
          var tags = [];
          tags.push(UserService.user.station);
          //延迟 60 秒
          setTimeout(function () {
            jpushService.setTagsWithAlias(tags, UserService.user.staff_id);
          }, 60000);

        } else if (event && event.resultCode == 0) {//设置标签别名成功
          $mysqlite.update($schema.TABLE_NAME_CONFIG, { tags: event.tags, alias: event.alias }).then(function (r) {
            $cordovaToast.showShortBottom("设置标签别名成功：" + JSON.stringify(r));
            //alert(JSON.stringify(r));
          }, function (err) {
            $cordovaToast.showShortBottom("设置标签别名失败：" + JSON.stringify(err));
            //alert(JSON.stringify(err))
          });
        }
      }
      var openNotificationInAndroidCallback = function (data) {
        var json = data;
        //window.alert('openNotificationInAndroidCallback=' + JSON.stringify(json));
        if (typeof data === 'string') {
          json = JSON.parse(data);
        }
        var id = json.extras['cn.jpush.android.EXTRA'].id;
        var type = json.extras['cn.jpush.android.EXTRA'].type;
        //window.alert(id + '=' + type);
        //服务端推送的时候可以加标记 以便客户端判断进入不同处理
        if (UserService.user && UserService.user.staff_id) {
          //新派单，先插入手机数据库，再进入详情页面
          //催单，直接进入详情页面
          if (type == 'S') {//新工单 刷新数据库
            $state.go('app.service.list', { state: 'B' });
          }
        } else
          $state.go('login');

      }
      var config = {
        stac: setTagsWithAliasCallback,
        oniac: openNotificationInAndroidCallback
      };
      jpushService.init(config);
      /** 推送初始化---end*/
    });

    // 检查更新
    function checkUpdate() {
      var url = CommonService.URL_UPDATE_VERJSON;
      //获取版本
      /** */
      return $myhttp.get(url).then(function (res) {
        if (!angular.isObject(res))
          res = JSON.parse(res);
        //如果本地于服务端的APP版本不符合
        if (res[0].verCode > CommonService.APP_VERSION) {
                 
          var platform = $cordovaDevice.getPlatform();
          if(platform=='iOS')
            $window.open(CommonService.URL_IPA); 
          else
            showUpdateConfirm(res);
        }
      });
    }

    // 显示是否更新对话框
    function showUpdateConfirm(result) {
      //此次更新配件产品等数据需要重新同步,请确保手机端不存在未提交成功的工单
      var template = '1.当前版本:' + CommonService.APP_VERSION + ';</br>2.新版本:' + result[0].verCode + ';'
      if (result[0].verName == 1 || result[0].verName == '1')
        template = template + '</br>3.此次更新数据需要重新同步,请确保手机端不存在未提交成功的工单';
      var confirmPopup = $ionicPopup.confirm({
        title: '版本升级',
        template: template, //从服务端获取更新的内容
        cancelText: '取消',
        okText: '升级'
      });
      confirmPopup.then(function (res) {
        if (res) {
          $ionicLoading.show({
            template: "已经下载：0%"
          });
          
          var url_apk = CommonService.URL_APK; //可以从服务端获取更新APP的路径
          //var targetPath = "/sdcard/Download/ionic.apk"; APP下载存放的路径，可以使用cordova file插件进行相关配置
          var targetPath = "file:///storage/sdcard0/Download/chinaefu5.apk";    
          
          var trustHosts = true
          var options = {};
          $cordovaFileTransfer.download(url_apk, targetPath, options, trustHosts).then(function (result) {
            // 打开下载下来的APP
            $cordovaFileOpener2.open(targetPath, 'application/vnd.android.package-archive').then(function () {
              //CommonService.showAlert('==open');
              // 成功
            }, function (err) {
              // 错误
            });
          }, function (err) {
            $ionicLoading.hide();
            alert('下载失败：', JSON.stringify(err));
          }, function (progress) {
            //进度，这里使用文字显示下载百分比
            $timeout(function () {
              var downloadProgress = (progress.loaded / progress.total) * 100;
              $ionicLoading.show({
                template: "已经下载：" + Math.floor(downloadProgress) + "%"
              });
              if (downloadProgress > 99) {
                $ionicLoading.hide();
              }
            })
          });
        } else {
          // 取消更新
        }
      });
    }
    //工单列表返回键处理
    window.addEventListener('native.keyboardhide', function (e) {
      cordova.plugins.Keyboard.isVisible = true;
      $timeout(function () {
        cordova.plugins.Keyboard.isVisible = false;
      }, 1000);
    });
    //物理返回按钮控制&双击退出应用
    $ionicPlatform.registerBackButtonAction(function (e) {
      //判断处于哪个页面时双击退出
      if (($location.path()).indexOf('/app/service/list/') >= 0) {
        CommonService.showConfirm('是否退出系统？').then(function (r) {
          var user = UserService.user;
          if (r) {
            var params = {
              _id: user.staff_id, tid: user.tid, uid: user.login_account,
              wcode: user.wcode, name: user.name
            };
            $myhttp.put(CommonService.URL_BASE + CommonService.URL_LOGOUT, params).then(function () {
              ionic.Platform.exitApp();
            });
          }

        });
      } else if ($ionicHistory.backView()) {
        if ($cordovaKeyboard.isVisible()) {
          $cordovaKeyboard.close();
        } else {
          $ionicHistory.goBack();
        }
      } else {
        if ($rootScope.backButtonPressedOnceToExit) {
          ionic.Platform.exitApp();
        } else {
          $rootScope.backButtonPressedOnceToExit = true;
          $cordovaToast.showShortBottom('再按一次退出系统');
          setTimeout(function () {
            $rootScope.backButtonPressedOnceToExit = false;
          }, 2000);
        }
      }
      e.preventDefault();
      return false;
    }, 101);

  }).run(['$rootScope', function ($rootScope) {
    Promise.setScheduler(function (cb) {
      $rootScope.$evalAsync(cb);
    });

  }]);

