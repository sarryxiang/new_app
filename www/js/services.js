var app = angular.module('app.services', []);

app.factory('$myhttp', function ($http, $location, $rootScope) {
  $http.defaults.cache = false;
  var service = {};
  service.get = function (url, params, cache) {
    if (!cache) {
      cache = false;
    }
    if (!params) {
      params = {};
    }
    params.timestamp = new Date().getTime();

    return new Promise(function (resolve, reject) {
      $http({ method: 'GET', url: url, cache: cache, params: params }).success(function (data) {
        //alert(JSON.stringify(data));
        if (data.error || data.err) {
          alert('http get err:'+JSON.stringify('get(' + url + ',' + JSON.stringify(params) + ')=' + JSON.stringify(data)));
          reject(data.error || data.err);
          console.log('get(' + url + ',' + JSON.stringify(params) + ')=' + data.error);
        } else if (data.result) {
          resolve(data.result);
        } else
          resolve(data);
      }).error(function (data, status) {
        if (status == '401') {
          $location.path('/');
          reject('');
        } else {
          reject('网络异常');
          console.log('get(' + url + ',' + JSON.stringify(params) + ')=网络异常');
        }
      });

    })
  };

  service.post = function (url, params, cache) {
    if (!cache) {
      cache = false;
    }
    return new Promise(function (resolve, reject) {
      $http({ method: 'POST', url: url, data: params, cache: cache }).success(function (data) {
        if (data.error || data.err) {
          reject(data.error || data.err);
          console.log('post(' + url + ',' + JSON.stringify(params) + ')=' + data.error);
        } else {
          resolve(data.result);
        }
      }).error(function (data, status) {
        if (status == '401') {
          $location.path('/');
          reject('');
        } else {
          reject('网络异常');
          console.log('post(' + url + ',' + JSON.stringify(params) + ')=网络异常');
        }
      });
    });
  };

  service.put = function (url, params, cache) {
    if (!cache) {
      cache = false;
    }
    return new Promise(function (resolve, reject) {
      $http({ method: 'PUT', url: url, data: params, cache: cache }).success(function (data) {
        //alert(JSON.stringify(data));
        if (data.error || data.err) {
          reject(data.error || data.err);
          console.log('put(' + url + ',' + JSON.stringify(params) + ')=' + data.error);
        } else {
          resolve(data.result);
        }
      }).error(function (data, status) {
        if (status == '401') {
          $location.path('/');
          reject('');
        } else {
          reject('网络异常');
          console.log('put(' + url + ',' + JSON.stringify(params) + ')=网络异常');
        }
      });
    });
  };

  service.delete = function (url, cache) {
    if (!cache) {
      cache = false;
    }
    return new Promise(function (resolve, reject) {
      $http({ method: 'DELETE', url: url, cache: cache }).success(function (data) {
        if (data.error || data.err) {
          reject(data.error || data.err);
          console.log('delete(' + url + ')=' + data.error);
        } else {
          resolve(data.result);
        }
      }).error(function (data, status) {
        reject('网络异常');
        console.log('delete(' + url + ')=网络异常');
        if (status == '401') {
          $location.path('/');
        }
      });
    });
  };


  service.jsonp = function (url, params) {
    if (!params) {
      params = { callback: 'JSON_CALLBACK' };
    } else {
      params.callback = 'JSON_CALLBACK';
    }
    params.timestamp = new Date().getTime();
    params.jsonp = 'jsonp';
    return new Promise(function (resolve, reject) {
      $http({ method: 'jsonp', url: url, cache: false, params: params })
        .success(function (data) {
          if (data.error) {
            reject(data.error);
          } else {
            resolve(data.result);
          }
        }).error(function () {
          reject('网络异常');
        });
    });
  };

  service.jsonp_ = function (url, params) {
    if (!params) {
      params = { callback: 'JSON_CALLBACK' };
    } else {
      params.callback = 'JSON_CALLBACK';
    }
    params.timestamp = new Date().getTime();
    params.jsonp = 'jsonp';
    return new Promise(function (resolve, reject) {
      $http({ method: 'jsonp', url: url, cache: false, params: params })
        .success(function (data) {
          resolve(data);
        }).error(function () {
          reject('网络异常');
        });
    });
  };

  return service;
});

app.factory('UserService', function ($myhttp, $q, $filter, $schema, $mysqlite) {
  var service = {};

  service.brandTypeChange = function (brand, category) {
        if (service.metadata) {
            return service.getBrandTypeChange(brand, category)
        } else {
            return $mysqlite.select('select * from ' + $schema.TABLE_NAME_METADATA + " where context like 'MACHINE.TYPE' or context like 'MACHINE.BRAND'").then(function (res){
              service.metadata = res.data;
              return service.getBrandTypeChange(brand, category);
            });
        }
    };

  service.getBrandTypeChange = function(brand, category){
    return new Promise(function (resolve, reject) {
      
      var sql_category = "select distinct(a.category_id) key,b.value value from " + $schema.TABLE_NAME_BRAND_CATEGORY + " a," + $schema.TABLE_NAME_METADATA
        + " b where a.category_id = b.key and a.brand_id = '" + brand + "' and b.context like 'MACHINE.TYPE'";
      var sql_brand = "select distinct(a.brand_id) key,b.value value from " + $schema.TABLE_NAME_BRAND_CATEGORY + " a," + $schema.TABLE_NAME_METADATA
        + " b where a.brand_id = b.key and a.category_id = '" + category + "' and b.context like 'MACHINE.BRAND'";
      
      var properties = undefined;//品牌品类对应的型号key
      var modeList = [];
      
      var rtn = {brand: brand, category: category};
      if (!brand && !category) {//都为“请选择” 默认全部值
        rtn.categoryList = $filter('filter')(service.metadata, { context: 'MACHINE.TYPE' });
        rtn.brandList = $filter('filter')(service.metadata, { context: 'MACHINE.BRAND' });
        resolve(rtn);
      } else if (brand && category && brand != '' && category != '') {//都有值,相互影响 筛选可选项
        Promise.resolve().then(function () {
          return $mysqlite.select(sql_category);
        }).then(function (res) {
          if (res)
            rtn.categoryList = res.data;
          return $mysqlite.select(sql_brand);
        }).then(function (r) {
          if (r)
            rtn.brandList = r.data;
          return $mysqlite.select("select properties from " + $schema.TABLE_NAME_BRAND_CATEGORY + 
            " where brand_id = '"+brand+"' and category_id = '"+category+"'");
        }).then(function(r){
          if(r && r.count > 0)
            return r.data;
          else 
            return [{}];
        }).each(function(p){
          if(p.properties){
            if(properties)
              properties = properties+","+p.properties.substring(1,p.properties.length-1);
            else
              properties = p.properties.substring(1,p.properties.length-1);
          }
        }).then(function(){
          if(properties)
            return $mysqlite.select("select key,value||'/'||remark as value,value as truevalue from " + $schema.TABLE_NAME_METADATA + 
              " where context like 'MACHINE.MODEL' and key in ("+properties+")");
        }).then(function(s){
          if(s && s.count > 0)
            rtn.modeList = s.data;
          resolve(rtn);
        }).catch(function (err) {
          reject(err);
        });
      } else if (!brand && category && category != '') {//品牌为“请选择”，品类有值，则重新筛选“品牌” “品类”默认全部值
        $mysqlite.select(sql_brand).then(function (res) {
          if (res)
            rtn.brandList = res.data;
          rtn.categoryList = $filter('filter')(service.metadata, { context: 'MACHINE.TYPE' });
          resolve(rtn);
        }).catch(function (err) {
          reject(err);
        });
      } else if (brand && brand != '' && !category) {//品类为“请选择”，品牌有值，则重新筛选“品类” “品牌”默认全部值
        $mysqlite.select(sql_category).then(function (res) {
          if (res)
            rtn.categoryList = res.data;
          rtn.brandList = $filter('filter')(service.metadata, { context: 'MACHINE.BRAND' });
          resolve(rtn);
        }).catch(function (err) {
          reject(err);
        });
      }
    }, function (error) {
      reject(error);
    });

  };

  return service;
});

//扫描、生成二维码
app.factory('$scancode', function ($myhttp, $cordovaBarcodeScanner) {
  var service = {};

  service.scan = function () {
    return new Promise(function (resolve, reject) {
      $cordovaBarcodeScanner.scan([]).then(function (barcodeData) {
        // {cancelled:false,text:'12345556665',format:'EAN_13'}
        resolve(barcodeData);
      }, function (error) {
        // An error occurred
        reject(error);
      });
    });
  };

  service.encode = function () {
    return new Promise(function (resolve, reject) {
      $cordovaBarcodeScanner.encode(BarcodeScanner.Encode.TEXT_TYPE, "http://www.nytimes.com").then(function (success) {
        // Success! Barcode data is here
        resolve();
      }, function (error) {
        // An error occurred
        reject(error);
      });
    });
  };

  return service;
});

//照片
app.factory('$mycamera', function ($myhttp, $cordovaCamera, $ionicActionSheet, $myFileTransfer, CommonService,
  $cordovaImagePicker) {
  var service = {};


  service.choosePicMenu = function (uid, tid) {
    return new Promise(function (resolve, reject) {


      $ionicActionSheet.show({
        buttons: [
          { text: '相机' },
          { text: '图库' }
        ],
        cancelText: '关闭',
        cancel: function () {
          return true;
        },
        buttonClicked: function (index) {
          switch (index) {
            case 0:
              takePhoto(uid, tid).then(function(r){
                resolve(r);
              })
              break;
            case 1:
              pickImage(uid, tid).then(function(r){
                resolve(r);
              });
              break;
            default:
              break;
          }
          return true;
        }
      });
    })
  }

  //图库选择
  var pickImage = function (uid, tid) {
    var uploadimages = [];
    var options = {
      maximumImagesCount: 5,
      width: 600,
      height: 600,
      quality: 80
    };
    return Promise.resolve().then(function () {
      return $cordovaImagePicker.getPictures(options);
    }).each(function (imageURI) {
      //alert(imageURI);
      return $myFileTransfer.upload(CommonService.URL_BASE + CommonService.URL_UPLOAD_PIC + '?uid=' + uid + '&tid=' + tid, imageURI)
        .then(function (r) {
          //上传成功，返回文件名
          if (!angular.isObject(r))
            r = JSON.parse(r);
          uploadimages.push({ filename: r.file, imageURI: imageURI });
        });

    }).then(function () {
      return uploadimages;
    });
  }

  //拍照
  var takePhoto = function (uid, tid) {
    var options = {
      //这些参数可能要配合着使用，比如选择了sourcetype是0，destinationtype要相应的设置
      quality: 100,//相片质量0-100
      destinationType: Camera.DestinationType.FILE_URI,//返回类型：DATA_URL= 0，返回作为 base64 編碼字串。 FILE_URI=1，返回影像档的 URI。NATIVE_URI=2，返回图像本机URI (例如，資產庫)
      sourceType: Camera.PictureSourceType.CAMERA,//从哪里选择图片：PHOTOLIBRARY=0，相机拍照=1，SAVEDPHOTOALBUM=2。0和1其实都是本地图库
      allowEdit: false,//在选择之前允许修改截图
      encodingType: Camera.EncodingType.JPEG,//保存的图片格式： JPEG = 0, PNG = 1
      targetWidth: 200,//照片宽度
      targetHeight: 200,//照片高度
      mediaType: 0,//可选媒体类型：圖片=0，只允许选择图片將返回指定DestinationType的参数。 視頻格式=1，允许选择视频，最终返回 FILE_URI。ALLMEDIA= 2，允许所有媒体类型的选择。
      cameraDirection: 0,//枪后摄像头类型：Back= 0,Front-facing = 1
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: true//保存进手机相册
    };
    var uploadimage;
    return Promise.resolve().then(function () {//alert('takePhoto');
      return $cordovaCamera.getPicture(options);
    }).then(function (imageURI) {//alert('takePhoto...'+imageURI);
      uploadimage = imageURI;
      return $myFileTransfer.upload(CommonService.URL_BASE + CommonService.URL_UPLOAD_PIC + '?uid=' + uid + '&tid=' + tid, imageURI);
    }).then(function (r) {
        //alert('takephoto...' + uploadimage);
        //上传成功，返回文件名
        if (!angular.isObject(r))
          r = JSON.parse(r);
        return [{ filename: r.file, imageURI: uploadimage }]
    });
  }


  service.choosePicMenu111 = function (uid, tid) {
    return new Promise(function (resolve, reject) {
      $ionicActionSheet.show({
        buttons: [
          { text: '拍照' },
          { text: '从相册选择' }
        ],
        titleText: '',
        cancelText: '取消',
        cancel: function () {
        },
        buttonClicked: function (index) {
          var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
          };
          if (index == 1)
            options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;

          $cordovaCamera.getPicture(options).then(function (imageURI) {
            //alert(JSON.stringify(imageURI));
            //var image = document.getElementById('myImage');
            //image.src = imageURI;
            $myFileTransfer.upload(CommonService.URL_BASE + CommonService.URL_UPLOAD_PIC + '?uid=' + uid + '&tid=' + tid, imageURI).then(function (r) {
              //上传成功，返回文件名
              if (!angular.isObject(r))
                r = JSON.parse(r);
              resolve({ filename: r.file, imageURI: imageURI });
            });
          }, function (err) {
            alert('image err:'+JSON.stringify(err));
            reject(err);
          });

          return true;
        }
      });
    });
  }


  return service;
});

//文件上传下载
app.factory('$myFileTransfer', function ($http, $location, $rootScope, $cordovaFileTransfer, $ionicLoading) {

  var service = {};

  service.upload = function (url, filePath, options) {
    $ionicLoading.show({
      template: '上传中...'
    });
    return new Promise(function (resolve, reject) {
      $cordovaFileTransfer.upload(url, filePath, options)
        .then(function (result) {
          //{response:{file:''},responseCode:200,objectId:'',bytesSent:12334}
          $ionicLoading.hide();
          if (!angular.isObject(result))
            result = JSON.parse(result);
          // Success!
          if (result.responseCode == 200)//上传成功，返回文件名
            resolve(result.response);
          else
            reject(result);
        }, function (err) {
          alert('upload err:' + JSON.stringify(err));
          $ionicLoading.hide();
          // Error
          reject(err);
        }, function (progress) {
          // constant progress updates
        });
    })
  };

  service.download = function (url, targetPath, options, trustHosts) {

    return new Promise(function (resolve, reject) {
      /**
      var url = "http://cdn.wall-pix.net/albums/art-space/00030109.jpg";
      var targetPath = cordova.file.documentsDirectory + "testImage.png";
      var trustHosts = true;
      var options = {};
       */

      $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
        .then(function (result) {
          // Success!
          resolve(result);
        }, function (err) {
          // Error
          reject(err);
        }, function (progress) {
          $timeout(function () {
            $scope.downloadProgress = (progress.loaded / progress.total) * 100;
          });
        });
    })
  };





  return service;
});

//极光推送
app.factory('jpushService', ['$http', '$window', '$document', function ($http, $window, $document) {
  var jpushServiceFactory = {};

  //启动极光推送
  var _init = function (config) {
    $window.plugins.jPushPlugin.init();
    //设置tag和Alias触发事件处理
    document.addEventListener('jpush.setTagsWithAlias', config.stac, false);
    //document.addEventListener('jpush.setAlias',config.stac,false);
    //打开推送消息事件处理
    $window.plugins.jPushPlugin.openNotificationInAndroidCallback = config.oniac;

    //调试模式 
    $window.plugins.jPushPlugin.setDebugMode(true);
  }
  //获取状态
  var _isPushStopped = function (fun) {
    $window.plugins.jPushPlugin.isPushStopped(fun);
  }
  //停止极光推送
  var _stopPush = function () {
    $window.plugins.jPushPlugin.stopPush();
  }

  //重启极光推送
  var _resumePush = function () {
    $window.plugins.jPushPlugin.resumePush();
  }
  //移除所有的本地通知
  var _clearLocalNotifications = function () {
    $window.plugins.jPushPlugin.clearLocalNotifications();
  }

  //设置标签和别名
  var _setTagsWithAlias = function (tags, alias) {
    //$window.plugins.jPushPlugin.setAliasAndTags(alias,tags);
    $window.plugins.jPushPlugin.setTagsWithAlias(tags, alias);
  }

  //设置标签
  var _setTags = function (tags) {
    $window.plugins.jPushPlugin.setTags(tags);
  }

  //设置别名
  var _setAlias = function (alias) {
    $window.plugins.jPushPlugin.setAlias(alias);
  }

  jpushServiceFactory.init = _init;
  jpushServiceFactory.isPushStopped = _isPushStopped;
  jpushServiceFactory.stopPush = _stopPush;
  jpushServiceFactory.resumePush = _resumePush;

  jpushServiceFactory.setTagsWithAlias = _setTagsWithAlias;
  jpushServiceFactory.setTags = _setTags;
  jpushServiceFactory.setAlias = _setAlias;
  jpushServiceFactory.clearLocalNotifications = _clearLocalNotifications;

  return jpushServiceFactory;
}]);
