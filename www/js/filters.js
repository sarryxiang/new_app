
var app = angular.module('app.filters', []);

app.filter('fromNow', function () {
    return function (date) {
        return moment(date).fromNow();
    }
});

app.filter('transMetadata', function ($filter) {
    return function (key, metadatas, context) {
        var metadata;
        if(metadatas)
            metadata = $filter('filter')(metadatas, {context: context, key: key});
        return metadata && metadata.length > 0 ? metadata[0].value : '';
    }
});

app.filter('date', function () {
    return function (date) {
        return moment(date).format('YYYY-MM-DD');
    }
});

app.filter('datetime', function () {
    return function (date) {
        return moment(date).format('YYYY-MM-DD HH:mm:ss');
    }
});
