/**
 * 配件加载页面/弹出层
 */
var app = angular.module('app');

app.controller('ItemSelectCtrl', function ($scope, $ionicModal, CommonService, $schema, $mysqlite) {

    /**配件加载页面/弹出层公用 start */
    $scope.itemList = [];
    $mysqlite.select('select * from ' + $schema.TABLE_NAME_ITEM).then(function (res) {
      if (res && res.count > 0) {
        $scope.itemList = res.data;
        //整理list key=item_id value=name（型号：model编码：code）
        for (var i=0; i < $scope.itemList.length; i++) {
          $scope.itemList[i].key = $scope.itemList[i].item_id;
          $scope.itemList[i].value = $scope.itemList[i].name + '（型号：' + $scope.itemList[i].model + '编码：'+ $scope.itemList[i].code + ")"; 
        }   
      }
    });

    //选中配件，关闭配件弹出层 并赋值
    $scope.selectedItem = function (ite) {
      if (!ite) {//请选择
        $scope.item.cur_select = { name: '选择配件', property: 'A', price: 0, count: 1, sellchannel: 'A' };
      } else
        $scope.item.cur_select = {
          item_id: ite.item_id, name: ite.name, model: ite.model, code: ite.code,
          property: ite.property, cost: ite.cost, price: ite.price, count: 1, sellchannel: 'A'
        };
    }

    //配件添加确认按钮
    $scope.addItem = function () {
      //把选择的配件$scope.item.cur_select添加到$scope.item.selected
      if ($scope.item.cur_select.item_id && $scope.item.cur_select.item_id != '') {
        $scope.item.selected.push($scope.item.cur_select);
        $scope.item.cur_select = { name: '选择配件', property: 'A', price: 0, count: 1, sellchannel: 'A' };
      } else
        CommonService.showAlert('请先选择配件');
    };
    //删除配件选择项
    $scope.delItem = function (index) {
      //从$scope.item.selected删除
      $scope.item.selected.splice(index, 1);
    };
    //清空已选择的配件 
    $scope.clearItem = function () {
      $scope.item.selected = [];
    };
    //配件数量增减按钮
    $scope.itemNumOption = function (type) {
      if (type == 'm' && $scope.item.cur_select.count > 1) {//减
        $scope.item.cur_select.count = $scope.item.cur_select.count - 1;
      } else if (type == 'p')//加
        $scope.item.cur_select.count = $scope.item.cur_select.count + 1;
    };
    /**配件加载页面/弹出层公用 end */


    /**弹出层使用的方法 start */
    //打开弹出层
    $scope.showModal = function(){
        $ionicModal.fromTemplateUrl('views/service/items_select.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.select_modal = modal;
            
            var itemobj = JSON.parse($scope.itemModal);
            $scope.item = {selected: itemobj.selected,
                remark: itemobj.remark,
                cur_select: { item_id: '', name: '', model: '', code: '', property: 'A', cost: 0, price: 0, count: 1, sellchannel: 'A' }};
                
            $scope.select_modal.show();
        });
    };
    //关闭弹出层
    $scope.closeModal = function () {
        $scope.select_modal.hide();
    };
    //弹出层中的“确定”按钮---调回调函数
    $scope.ok = function(){
        $scope.callbackOption($scope.item);
        $scope.select_modal.hide();
    };
    /**弹出层使用的方法 end */

});