var app = angular.module('app');

//登录
app.controller('LoginCtrl', function ($scope, $stateParams, $state, $myhttp, $schema, CommonService, UserService, $mysqlite, 
  $ionicLoading, $cordovaNetwork, jpushService, $cordovaToast, $cordovaDevice) {
  $scope.loginFlag = {loginState: false, loginIcon: false};
  $scope.isChecked = true;
  /**
  $state.go('app.service.list', { state: 'B' });
  $scope.user = { tenant_id: CommonService.TENANT_ID, login_account: CommonService.LOGIN_ACCOUNT, password: CommonService.PASSWORD, tid: '50005' };
  UserService.user = $scope.user; */
  /** */

  $ionicLoading.show({ template: '页面加载中...' });
  $mysqlite.initConfig().then(function(){
    $ionicLoading.hide();
    $scope.user = { tenant_id: CommonService.TENANT_ID, login_account: CommonService.LOGIN_ACCOUNT, 
      tags: CommonService.JPUSH_TAGS, alias: CommonService.JPUSH_ALIAS, password: CommonService.PASSWORD };
  });

  $scope.login = function () {
    if(!$scope.user.login_account || !$scope.user.password || !$scope.user.tenant_id)
      CommonService.showAlert('请完善登录信息');
    else{
      $scope.loginFlag.loginIcon = true;
      if ($cordovaNetwork.isOnline()) {
        $myhttp.get(CommonService.URL_GET_TID + $scope.user.tenant_id).then(function (res) {//根据网点标识获取数据库（tid）
          //alert('dbid='+JSON.stringify(res));
          if (res && res.dbid) {
            $scope.user.tid = res.dbid;
            return $myhttp.get(CommonService.URL_BASE + CommonService.URL_LOGIN, { uid: $scope.user.login_account, passwd: $.md5($scope.user.password), tid: res.dbid });
          } else {
            $scope.loginFlag.loginIcon = false;
            CommonService.showAlert('网点不存在');
          }
        }).then(function (r) {
          //alert('login='+JSON.stringify(r));
          if (r && r.user && r.user._id) {//登录成功
            $scope.loginFlag.loginState = true;
            $scope.user.staff_id = r.user._id;
            $scope.user.wcode = r.user.wcode;
            $scope.user.name = r.user.name;
            $scope.user.station = r.user.station;
            UserService.user = $scope.user;
            
            //移除本地所有通知
            //jpushService.clearLocalNotifications();
            //注册接收推送标识 start
            //設置接受推送的标签和别名['5537d54cad59dc400768edc1'],'5537d54cad59dc400768edc2'
            if($scope.user.tags && $scope.user.alias && $scope.user.tags==('['+$scope.user.station+']') && 
              $scope.user.alias==$scope.user.staff_id){
                $cordovaToast.showShortBottom("您的标签别名：" + $scope.user.tags + '#' + $scope.user.alias);
            }else{//当前登录员工的标签和别名设置
              var tags = [];
              tags.push($scope.user.station);
              jpushService.setTagsWithAlias(tags, $scope.user.staff_id);
            }
            //注册接收推送标识 end

            var params = { staff_id: r.user._id, wcode: r.user.wcode, tenant_id: $scope.user.tenant_id, station: $scope.user.station, tid: $scope.user.tid, login_account: '', password: '' };
            if ($scope.isChecked) {//记住账号密码
              params.login_account = $scope.user.login_account;
              params.password = $scope.user.password;
            } else {
              params.login_account = '';
              params.password = '';
            }
            return $mysqlite.update($schema.TABLE_NAME_CONFIG, params);
          }
        }).then(function (result) {//{"rows":{"length":0},"rowsAffected":1,"insertId":1}
          //alert('update config...'+JSON.stringify(result));
          if ($scope.loginFlag.loginState) {
            var par = {
              state: "B",
              deal_staff: $scope.user.staff_id,
              tid: $scope.user.tid,
              uid: $scope.user.login_account,
              order: "callback_date:asc",
              pageNumber: 1,
              pageSize: CommonService.PAGE_SIZE
            };
            return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_SERVICELIST, par);
          }
        }).then(function () {
          $scope.loginFlag.loginIcon = false;
          if ($scope.loginFlag.loginState)
            $state.go('app.service.list', { state: 'B' });
        }).catch(function (err) {
          $scope.loginFlag = {loginState: false, loginIcon: false};
          CommonService.showAlert('err:' + JSON.stringify(err));
        });
      } else {
        CommonService.showConfirm('当前网络未连接，是否继续离线操作？').then(function (r) {
          if (r) {//离线操作 访问本地数据
            $scope.loginFlag.loginIcon = true;
            $mysqlite.select('select * from ' + $schema.TABLE_NAME_CONFIG + " where login_account='" + $scope.user.login_account + "' and password='" + $scope.user.password + "'").then(function (result) {
              if (result.count > 0) {
                var r = result.data[0];
                $scope.loginFlag.loginState = true;
                $scope.user.staff_id = r.user._id;
                $scope.user.wcode = r.user.wcode;
                $scope.user.name = r.user.name;
                UserService.user = $scope.user;
                var params = { login_account: '', password: '' };
                if ($scope.isChecked) {//记住账号密码
                  params.login_account = $scope.user.login_account;
                  params.password = $scope.user.password;
                } else {
                  params.login_account = '';
                  params.password = '';
                }
                return $mysqlite.update($schema.TABLE_NAME_CONFIG, params);
              } else {
                $scope.loginFlag.loginIcon = false;
                CommonService.showAlert('登录账户与密码不匹配');
              }

            }).then(function (r) {
              $scope.loginFlag.loginIcon = false;
              if ($scope.loginFlag.loginState)
                $state.go('app.service.list', { state: 'B' });
            }).catch(function (err) {
              $scope.loginFlag = {loginState: false, loginIcon: false};
              CommonService.showAlert('err:' + JSON.stringify(err));
            });
          }else
            $scope.loginFlag.loginIcon = false;
        });
      }
    }

  };


});
