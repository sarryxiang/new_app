var app = angular.module('app');

//左侧菜单
app.controller('MenuCtrl', function ($scope, $stateParams, $timeout, $state, CommonService, UserService, $schema,
  $mysqlite, $myhttp, $ionicLoading, $cordovaNetwork, $cordovaDevice, $interval, $rootScope, $cordovaToast
  , $cordovaGeolocation) {

  $scope.user = UserService.user;
  var posOptions = {timeout: 10000, enableHighAccuracy: false};
  var platform = $cordovaDevice.getPlatform();
   if(platform=='iOS'){
     var timer = $interval(function () {
        /** 地图定位 发送到服务端*/
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (data) {
          var latitude  = data.coords.latitude;
          var lontitude = data.coords.longitude;
          CommonService.LOCATION_LAT = latitude;
          CommonService.LOCATION_LNG = lontitude;
          if (latitude && lontitude){
            $scope.params = {
              _id: $scope.user.staff_id, tid: $scope.user.tid, uid: $scope.user.login_account,
              wcode: $scope.user.wcode, name: $scope.user.name, latitude: latitude, lontitude: lontitude
            };
            $mysqlite.selectOne("select * from " + $schema.TABLE_NAME_CONFIG).then(function (res) {
              $scope.params.num = res.rf_count;//alert('location:'+CommonService.URL_BASE + CommonService.URL_SET_LOCATION+'='+JSON.stringify($scope.params));
              return $myhttp.put(CommonService.URL_BASE + CommonService.URL_SET_LOCATION, $scope.params);
            }).then(function (result) {
              //alert('post location return='+JSON.stringify(result));
            });
          }
        }, function (err) {
          $cordovaToast.showShortBottom("定位错误：" + JSON.stringify(err));
          alert("定位错误：" + JSON.stringify(err));
        });
      }, 60 * 1000);//3分钟180 * 1000
   } else {//android
     var timer = $interval(function () {
        /** 地图定位 发送到服务端*/
        baidu_location.getCurrentPosition(function (data) {
          var indexLat = data.indexOf('latitude');
          var indexLng = data.indexOf('lontitude');
          var indexRad = data.indexOf('radius');
          var latitude = data.substring(indexLat + 10, indexLng);
          var lontitude = data.substring(indexLng + 11, indexRad);
          CommonService.LOCATION_LAT = latitude;
          CommonService.LOCATION_LNG = lontitude;
          if (latitude&&latitude.indexOf('E')<0 && lontitude&&lontitude.indexOf('E')<0){
            $scope.params = {
              _id: $scope.user.staff_id, tid: $scope.user.tid, uid: $scope.user.login_account,
              wcode: $scope.user.wcode, name: $scope.user.name, latitude: latitude, lontitude: lontitude
            };
            $mysqlite.selectOne("select * from " + $schema.TABLE_NAME_CONFIG).then(function (res) {
              $scope.params.num = res.rf_count;//alert('location:'+CommonService.URL_BASE + CommonService.URL_SET_LOCATION+'='+JSON.stringify($scope.params));
              return $myhttp.put(CommonService.URL_BASE + CommonService.URL_SET_LOCATION, $scope.params);
            }).then(function (result) {
              //alert('post location return='+JSON.stringify(result));
            });
          }
        }, function (err) {
          $cordovaToast.showShortBottom("定位错误：" + JSON.stringify(err));
          alert("定位错误：" + JSON.stringify(err));
        });
      }, 60 * 1000);//3分钟180 * 1000

   }
          
  //退出
  $scope.logout = function () {
    CommonService.showConfirm('是否退出系统？').then(function (r) {
      if (r) {
        var params = {
          _id: $scope.user.staff_id, tid: $scope.user.tid, uid: $scope.user.login_account,
          wcode: $scope.user.wcode, name: $scope.user.name
        };
        $myhttp.put(CommonService.URL_BASE + CommonService.URL_LOGOUT, params).then(function () {
          $interval.cancel(timer);//---退出是是否停止
          if(platform=='iOS')
            $state.go('login');
          else
            ionic.Platform.exitApp();
        });
      }
    });
  }

  //同步数据 start
  $scope.sysData = function (type) {
    $scope.user = UserService.user;
    $scope.params = {
      tid: $scope.user.tid,
      uid: $scope.user.login_account
    };
    if ($cordovaNetwork.isOnline()) {
      $ionicLoading.show({ template: '数据同步中...' });
      if (type == 'S') {//工单数据刷新
        $scope.params.state = "B";
        $scope.params.deal_staff = $scope.user.staff_id;
        $scope.params.order = "callback_date:asc";
        $scope.params.pageNumber = 1;
        $scope.params.pageSize = CommonService.PAGE_SIZE;
        var rf_count;
        $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_SERVICELIST, $scope.params).then(function (servicelist) {
          if (servicelist && servicelist.data) {
            rf_count = servicelist.count;
            return $mysqlite.refreshDb($schema.TABLE_NAME_SERVICE, $schema.T_STRUCTURE_SERVICE, servicelist.data, type);
          }
        }).then(function () {
          if (rf_count)
            return $mysqlite.update($schema.TABLE_NAME_CONFIG, { rf_count: rf_count });
        }).then(function () {
          $ionicLoading.hide();
          CommonService.showAlert('工单刷新成功');
        }).catch(function (err) {
          $ionicLoading.hide();
          CommonService.showAlert('err=' + JSON.stringify(err));
        });
      } else if (type == 'B') {//基础数据同步
        $scope.params.context = "SERVICE.PROJECT";
        //维修项目SERVICE.PROJECT
        $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params).then(function (projectlist) {
          if (projectlist && projectlist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, projectlist, type, 'SERVICE.PROJECT');
        }).then(function () {//完成度SERVICE.PROCESS
          $scope.params.context = "SERVICE.PROCESS";
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params);
        }).then(function (processlist) {
          if (processlist && processlist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, processlist, type, 'SERVICE.PROCESS');
        }).then(function () {//维修性质SERVICE.SELLCHANNEL
          $scope.params.context = "SERVICE.SELLCHANNEL";
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params);
        }).then(function (sellchannellist) {
          if (sellchannellist && sellchannellist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, sellchannellist, type, 'SERVICE.SELLCHANNEL');
        }).then(function () {//工单来源SERVICE.SOURCE
          $scope.params.context = "SERVICE.SOURCE";
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params);
        }).then(function (sourcelist) {
          if (sourcelist && sourcelist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, sourcelist, type, 'SERVICE.SOURCE');
        }).then(function () {//工单子状态SERVICE.SUB_STATE
          $scope.params.context = "SERVICE.SUB_STATE";
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params);
        }).then(function (substatelist) {
          if (substatelist && substatelist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, substatelist, type, 'SERVICE.SUB_STATE');
        }).then(function () {//部门
          $scope.params.staff_id = $scope.user.staff_id;
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_DEPARTMENT, $scope.params);
        }).then(function (department) {
          if (department) {
            var ds = [];
            ds.push(department);
            return $mysqlite.refreshDb($schema.TABLE_NAME_DEPARTMENT, $schema.T_STRUCTURE_DEPARTMENT, ds, type);
          }
        }).then(function () {
          $ionicLoading.hide();
          CommonService.showAlert('基础数据同步成功');
        }).catch(function (err) {
          $ionicLoading.hide();
          CommonService.showAlert('err=' + JSON.stringify(err));
        });

      } else if (type == 'I') {//配件同步 循环获取
        //var datas = [];
        var page = 1;
        var totals = [];

        $scope.params.page_number = 1;
        $scope.params.page_size = 500;
        $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_ITEMS, $scope.params).then(function (items) {
          //alert(JSON.stringify(items));
          if (items && items.total > 0) {
            //datas.concat(items.data);
            page = items.page;
            totals = new Array(items.total);
            return $mysqlite.updateDbItems($schema.TABLE_NAME_ITEM, $schema.T_STRUCTURE_ITEM, items.data, items.count, $scope.params.page_number);
          }
        }).then(function () {
          return totals;
        }).each(function () {
          if (totals.length > 1) {
            $scope.params.page_number = page + 1;
            return Promise.resolve().then(function () {
              return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_ITEMS, $scope.params);
            }).then(function (its) {
              page = its.page;
              //datas.concat(its.data);
              //return datas;
              return $mysqlite.updateDbItems($schema.TABLE_NAME_ITEM, $schema.T_STRUCTURE_ITEM, its.data, its.count, $scope.params.page_number);
            });

          }
        }).then(function () {
          //return $mysqlite.refreshDb($schema.TABLE_NAME_ITEM, $schema.T_STRUCTURE_ITEM, datas, type);
        }).then(function () {
          $ionicLoading.hide();
          CommonService.showAlert('配件同步成功');
        }).catch(function (err) {
          $ionicLoading.hide();
          CommonService.showAlert('err=' + JSON.stringify(err));
        });

      } else if (type == 'G') {//产品数据同步
        $scope.params.context = "MACHINE.BRAND";
        //产品类型MACHINE.BRAND
        $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params).then(function (brandlist) {
          if (brandlist && brandlist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, brandlist, type, 'MACHINE.BRAND');
        }).then(function () {//产品品牌MACHINE.TYPE
          $scope.params.context = "MACHINE.TYPE";
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params);
        }).then(function (typelist) {
          if (typelist && typelist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, typelist, type, 'MACHINE.TYPE');
        }).then(function () {//产品型号MACHINE.MODEL
          $scope.params.context = "MACHINE.MODEL";
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_METADATA, $scope.params);
        }).then(function (modelist) {
          if (modelist && modelist.length > 0)
            return $mysqlite.refreshDb($schema.TABLE_NAME_METADATA, $schema.T_STRUCTURE_METADATA, modelist, type, 'MACHINE.MODEL');
        }).then(function () {//产品品类扩展字段
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_SETTING, $scope.params);
        }).then(function (setting) {
          if (setting) {
            var ss = [];
            ss.push(setting);
            return $mysqlite.refreshDb($schema.TABLE_NAME_SETTING, $schema.T_STRUCTURE_SETTING, ss, type);
          }
        }).then(function () {//产品品牌型号关联表--brand_category
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_BRAND_CATEGORY, $scope.params);
        }).then(function (bclist) {//alert(JSON.stringify(bclist));
          if (bclist && bclist.length > 0) {
            return $mysqlite.refreshDb($schema.TABLE_NAME_BRAND_CATEGORY, $schema.T_STRUCTURE_BRAND_CATEGORY, bclist, type);
          }
        }).then(function () {//产品品牌型号关联表
          return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_SERVICEFEE, $scope.params);
        }).then(function (sfeelist) {
          if (sfeelist && sfeelist.length > 0) {
            return $mysqlite.refreshDb($schema.TABLE_NAME_SERVICEFEE, $schema.T_STRUCTURE_SERVICEFEE, sfeelist, type);
          }
        }).then(function () {
          $ionicLoading.hide();
          CommonService.showAlert('产品数据同步成功');
        }).catch(function (err) {
          $ionicLoading.hide();
          CommonService.showAlert('err=' + JSON.stringify(err));
        });
      }
    } else
      CommonService.showAlert('当前网络未连接');
  }
  //同步数据 end

});
