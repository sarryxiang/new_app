/**
 * 图片加载页面/弹出层
 */
var app = angular.module('app');

app.controller('ImageSelectCtrl', function ($scope, $ionicModal, CommonService, $schema, $mysqlite, $mycamera, UserService) {
    
    /**弹出层专用 start */
    $scope.beforeNewImages = undefined;//当前弹出层之前新增加的图片
    /**弹出层中的操作按钮 type=1提交、type=2跳过 */
    $scope.ok = function(type){
        if(type == 2)
            $scope.callbackOption(JSON.parse($scope.beforeNewImages));
        else
            $scope.callbackOption($scope.newImages);
    };
    $scope.$watch('newImages',function(){
        if($scope.newImages && !$scope.beforeNewImages)
            $scope.beforeNewImages = JSON.stringify($scope.newImages);
    });
    /**弹出层专用 end */

    //上传图片
    $scope.slideImages = [];//整合新上传图片 和 工单原有图片，方便滑动查看
    $scope.imageUrl = CommonService.URL_BASE + CommonService.URL_UPLOAD_PIC + '/';
    $scope.imageParams = '?uid=' + UserService.user.login_account + '&tid=' + UserService.user.tid;
    $scope.choosePicMenu = function () {
        $mycamera.choosePicMenu(UserService.user.login_account, UserService.user.tid).then(function (files) {
        //alert('rfmod files...'+JSON.stringify(files));
        return files;
        }).each(function(file){ 
        $scope.newImages.push({ name: file.filename, imageURI: file.imageURI });
        });
    };
    //删除图片
    $scope.delImage = function (i) {
        CommonService.showConfirm('是否删除此图片？').then(function (r) {
        if (r)
            $scope.newImages.splice(i, 1);
        });
    }
    //清空图片
    $scope.clearImage = function () {
        CommonService.showConfirm('确定清空图片？').then(function (r) {
        if (r)
            $scope.newImages = [];
        });
    }//当前放到图片的位置index
    $scope.bigImageIndex = 0;
    $scope.bigImage = false;
    //点击图片放大  
    $scope.shouBigImage = function (url, index, type) {
        new Promise.resolve().then(function () {
        return $scope.newImages;
        }).each(function (e) {
        $scope.slideImages.push({ imageURI: e.imageUrl });
        }).then(function () {
        return $scope.oldImages;//$scope.service.attachments
        }).each(function (e) {
        $scope.slideImages.push({ imageURI: $scope.imageUrl + e.name + $scope.imageParams });
        }).then(function () {
        //传递一个参数（图片的URl）  
        $scope.Url = url;                   //$scope定义一个变量Url，这里会在大图出现后再次点击隐藏大图使用  
        $scope.bigImage = true;                   //显示大图 
        if (type == 1) //新上传图片
            $scope.bigImageIndex = index;
        else//原图片
            $scope.bigImageIndex = $scope.newImages.length + index;
        });

    };
    $scope.hideBigImage = function () {
        $scope.bigImage = false;
    }
    $scope.onDragLeft = function () {//向左滑动
        if ($scope.bigImageIndex < $scope.slideImages.length - 1) {
        $scope.bigImageIndex = $scope.bigImageIndex + 1;
        $scope.Url = $scope.slideImages[$scope.bigImageIndex].imageURI;
        }
    }
    $scope.onSwipeRight = function () {//向右滑动
        if ($scope.bigImageIndex > 0) {
        $scope.bigImageIndex = $scope.bigImageIndex - 1;
        $scope.Url = $scope.slideImages[$scope.bigImageIndex].imageURI;
        }
    }
});