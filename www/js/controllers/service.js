/**
 * 维修单操作
 */
var app = angular.module('app');

app.controller('ServiceListCtrl', function ($scope, $stateParams, $state, $myhttp, $schema, $mysqlite, UserService, CommonService, $ionicLoading) {
  $ionicLoading.show({ template: '数据加载中...' });
  var state = $stateParams.state;
  $scope.servicelist = [];//[{ state: '已派单', service_id: 'WX-201609063382', user_name: '张桂香' }, { state: '已派单', service_id: 'WX-201609063381', user_name: '王晶' }];
  $scope.user = UserService.user;
  /** */
  var rf_count;
  $scope.metadatas = [];
  $mysqlite.select('select * from ' + $schema.TABLE_NAME_METADATA).then(function (res) {
      if (res && res.count > 0) {
        $scope.metadatas = res.data;
      }
      if(state == 3 || state == 4){//未预约3、预约今天4===查询本地数据库
        var where = " datetime(order_at) >= datetime('"+moment().format('YYYY-MM-DD 00:00:00')+"') and datetime(order_at) <= datetime('"+moment().format('YYYY-MM-DD 23:59:59')+"')";
        if(state == 3)//未预约
          where = " order_at='' or order_at='undefined' or order_at=null or order_at='null'";
        return $mysqlite.select('select * from ' + $schema.TABLE_NAME_SERVICE + " where" + where);
      }else{
        var par = {
          state: state,
          deal_staff: $scope.user.staff_id,
          tid: $scope.user.tid,
          uid: $scope.user.login_account,
          order: "callback_date:asc",
          pageNumber: 1,
          pageSize: CommonService.PAGE_SIZE
        };
        return $myhttp.get(CommonService.URL_BASE + CommonService.URL_GET_SERVICELIST, par);
      }
  }).then(function(servicelist){
    //alert('servicelist='+JSON.stringify(servicelist));//{count:1,total:1,page:1,data:[{}]}
    $ionicLoading.hide();
    if (servicelist && servicelist.data){
      $scope.servicelist = servicelist.data;
      rf_count = servicelist.count;
      if(state != 3 && state != 4)//从服务端获取的工单数据，刷新手机端数据库
        return $mysqlite.refreshDb($schema.TABLE_NAME_SERVICE, $schema.T_STRUCTURE_SERVICE, servicelist.data, 'S');
    }
  }).then(function () {
    if(state != 3 && state != 4 && rf_count)
      return $mysqlite.update($schema.TABLE_NAME_CONFIG, {rf_count: rf_count});
  }).catch(function (err) {
    $ionicLoading.hide();
    CommonService.showAlert('工单查询err:' + JSON.stringify(err));
  });
  

  $scope.servicemodify = function (service) {
    $state.go('app.service.modify', { _id: service._id });
  }

});

app.controller('ServiceModifyCtrl', function ($scope, $stateParams, $ionicLoading, $ionicModal, $state, $ionicActionSheet,
  ionicTimePicker, ionicDatePicker, $filter, $myhttp, CommonService, UserService, $schema, $mysqlite, $scancode, $mycamera, 
  $cordovaNetwork, $ionicScrollDelegate) {
  var _id = $stateParams._id;
  $scope.service = {};//{ service_id: '5678', call_caller: '1345', user_address: '啦啦啦' };
  //工单分块显示 flags控制每块的展开、收缩
  $scope.flags = { orderFlag: true, goodsFlag: false, stateFlag: false, itemFlag: false};
  //品类扩展信息 从数据库表setting中传值category查询获得
  $scope.categoryExtend = { extend2: undefined, extend2scan: 'N',
    extend3: undefined, extend3scan: 'N', extend4: undefined, extend4scan: 'N', extend5: undefined, extend5scan: 'N'
  };
  $scope.projectList = [];//维修项目
  //品类
  $scope.categoryList =[];
  //品牌
  $scope.brandList = [];
  //$scope.stateList = CommonService.STATE_LIST;//状态
  $scope.resultList = CommonService.RESULT_LIST;//处理结果
  //配件
  //item_id@价格price@数量@名字"name"+"model"+"编码:"+"code"@实虚核property@保内外@cost成本价
  /** 配件：selected显示已选择的配件；
   * cur_select当前选择的配件（name: 名称 count: 选择数量, price: 价格, sellchannel: 保内1外2,property:实核A虚核B）
   * */
  $scope.item = {
    selected: [],
    cur_select: { item_id: '', name: '', model: '', code: '', property: 'A', cost: 0, price: 0, count: 1, sellchannel: 'A' }
  };

  $scope.metadatas = [];
  $scope.oldItems = [];
  /**根据id查维修单详情
   * 获取metadata基础数据（产品类型MACHINE.TYPE;产品品牌MACHINE.BRAND;维修项目SERVICE.PROJECT；完成度SERVICE.PROCESS;维修性质SERVICE.SELLCHANNEL）
   * 获取所有配件
   * 初始化品牌品类列表*/
  $scope.init = function () {
    $mysqlite.select('select * from ' + $schema.TABLE_NAME_METADATA).then(function (res) {//alert('metadata'+JSON.stringify(res));
      if (res && res.count > 0) {
        $scope.metadatas = res.data;
        $scope.categoryList = $filter('filter')($scope.metadatas, { context: 'MACHINE.TYPE' });//过滤metadatas中context='MACHINE.TYPE'
        $scope.brandList = $filter('filter')($scope.metadatas, { context: 'MACHINE.BRAND' });//过滤metadatas中context='MACHINE.BRAND'
        $scope.sellchannelList = $filter('filter')($scope.metadatas, { context: 'SERVICE.SELLCHANNEL' });
        $scope.subStateList = $filter('filter')($scope.metadatas, { context:'SERVICE.SUB_STATE',app_state:'Y' });
        $scope.processList = $filter('filter')($scope.metadatas, { context:'SERVICE.PROCESS' });
        $scope.sourceList = $filter('filter')($scope.metadatas, { context:'SERVICE.SOURCE' });
      }
      return $mysqlite.selectOne('select * from ' + $schema.TABLE_NAME_SERVICE + " where _id = '" + _id + "'");
    }).then(function (data) {
      if (data && data.service_id){
        $scope.service = data;
        $scope.item.remark = $scope.service.remark;
        if($scope.service.attachments)//图片
          $scope.service.attachments = JSON.parse($scope.service.attachments);
        //预约时间
        if($scope.service.order_at){
          $scope.service.order_at_date = moment($scope.service.order_at).format('YYYY-MM-DD');
          $scope.service.order_at_time = moment($scope.service.order_at).format('HH:mm:ss');
        }
        //派单时间
        if($scope.service.dispatch_at){
          $scope.service.dispatch_at_date = moment($scope.service.dispatch_at).format('YYYY-MM-DD');
          $scope.service.dispatch_at_time = moment($scope.service.dispatch_at).format('HH:mm:ss');
        }
        //历史配件
        if($scope.service.old_items)
          $scope.oldItems = JSON.parse($scope.service.old_items);
      }
      $scope.getBrandOrCategory('','f');

      //初始化 $scope.$watch这种在工单、基础数据查询完之后再调用
      // 维修性质变化 联动 维修项目、维修费和路费
      $scope.$watch('service.property', function () {
        if ($scope.service.property && $scope.service.property != '')
          $scope.getProjectList().then(function () {
            return $scope.getFee();
          });
      });
      // 维修项目变化 联动 维修费和路费
      $scope.$watch('service.project', function () {
        if ($scope.service.project && $scope.service.project != '')
          $scope.getFee();
      });
      //获取品类扩展字段
      $scope.$watch('service.category', function () {
        $scope.categoryExtend = { extend2: undefined, extend2scan: 'N',
          extend3: undefined, extend3scan: 'N', extend4: undefined, extend4scan: 'N', extend5: undefined, extend5scan: 'N'
        };
        if ($scope.service.category && $scope.service.category != '') {
          $mysqlite.select('select * from ' + $schema.TABLE_NAME_SETTING + " where category = '" + $scope.service.category + "'").then(function (res) {
            if (res && res.count > 0)
              return res.data;
            else
              return [];
          }).each(function (r) {
            for (var key in $scope.categoryExtend) {
              if (key == r.val) {
                $scope.categoryExtend[key] = r.view;
                $scope.categoryExtend[key + 'scan'] = r.scan;
              }
            }
          }).catch(function (err) {
            CommonService.showAlert('err:' + JSON.stringify(err));
          });
        }
      });
      //20140812 状态为缺件待修，处理结果自动变为申购配件
      $scope.$watch('service.subState', function () {
        if ($scope.service.subState == 'D')
          $scope.service.result_type = 'B';
      });

      //费用相关
      if(!angular.isNumber($scope.service.fact_fee))
          $scope.service.fact_fee = 0;
      $scope.$watch('fee', function () {
        if(!angular.isNumber($scope.service.fee))
          $scope.service.fee = 0;
        $scope.service.fact_fee = $scope.service.fact_fee + $scope.service.fee;
      });
      $scope.$watch('road_fee', function () {
        if(!angular.isNumber($scope.service.road_fee))
          $scope.service.road_fee = 0;
        $scope.service.fact_fee = $scope.service.fact_fee + $scope.service.road_fee;
      });
      $scope.$watch('item_fee', function () {
        if(!angular.isNumber($scope.service.item_fee))
          $scope.service.item_fee = 0;
        $scope.service.fact_fee = $scope.service.fact_fee + $scope.service.item_fee;
      });
      $scope.$watch('remote_fee', function () {
        if(!angular.isNumber($scope.service.remote_fee))
          $scope.service.remote_fee = 0;
        $scope.service.fact_fee = $scope.service.fact_fee + $scope.service.remote_fee;
      });
      $scope.$watch('carry_fee', function () {
        if(!angular.isNumber($scope.service.carry_fee))
          $scope.service.carry_fee = 0;
        $scope.service.fact_fee = $scope.service.fact_fee + $scope.service.carry_fee;
      });
      $scope.$watch('other_fee_s', function () {
        if(!angular.isNumber($scope.service.other_fee_s))
          $scope.service.other_fee_s = 0;
        $scope.service.fact_fee = $scope.service.fact_fee + $scope.service.other_fee_s;
      });

    }).catch(function (err) {
      CommonService.showAlert('err:' + JSON.stringify(err));
    });
  };
  $scope.init();

  //扫描:出厂编码、扩展字段（室外机条码） type类型(e扩展字段)、index(type为扩展字段是，index为扩展字段序号)
  $scope.scan = function (type, index) {
    $scancode.scan().then(function (qrcode) {
      if (type == 'qr')
        $scope.service.qr_code = qrcode.text;
      else if (type == 'e') {//扫描值赋给扩展字段
        if (index == 1)
          $scope.service.extend1 = qrcode.text;
        else if (index == 2)
          $scope.service.extend2 = qrcode.text;
        else if (index == 3)
          $scope.service.extend3 = qrcode.text;
        else if (index == 4)
          $scope.service.extend4 = qrcode.text;
        else if (index == 5)
          $scope.service.extend5 = qrcode.text;
      }

    });
  }
  
  //上传图片
  $scope.newImages = [];

  //品牌品类联动--type='f'代表初始化
  $scope.getBrandOrCategory = function (obj, type) {
    var key = '';
    if(obj && obj != '' && type != 'f')
      key = obj.key;
    if(type == 'b')
      $scope.service.brand = key;
    else if(type == 'c')
      $scope.service.category = key;
    
    $scope.modeList = [];
    UserService.brandTypeChange($scope.service.brand, $scope.service.category).then(function(res){
      if(type!='f'){
        $scope.brandList = res.brandList;
        $scope.categoryList = res.categoryList;
      }
      if(res.modeList && res.modeList.length > 0){
        //对应可下拉的型号，服务端有些单子存错了，把value值存在了model字段里，这俩暂时处理翻译显示一下
        if($scope.service.model && $scope.service.model != ''){
          var m = $filter('filter')(res.modeList, {truevalue: $scope.service.model});
          if (m && m.length > 0){
            $scope.service.model2 = m[0].key;
            $scope.service.model_value = m[0].truevalue;
          }else{
            var m2 = $filter('filter')(res.modeList, {key: $scope.service.model});
            if(m2 && m2.length > 0){
              $scope.service.model2 = $scope.service.model;
              $scope.service.model_value = m2[0].truevalue;
            }else{
              $scope.service.model2 = '';
              $scope.service.model_value = $scope.service.model;
            }
          }
        }else{
          $scope.service.model2 = '';
          $scope.service.model_value = $scope.service.model;
        }
        
        $scope.modeList = res.modeList;
      }
      //联动维修项目
      return $scope.getProjectList();
    }).then(function(){//联动价格
        return $scope.getFee();
    }).catch(function (err) {
      CommonService.showAlert('err:' + JSON.stringify(err));
    });
    
  };

  /**查询维修项目：品类、品牌、维修性质三项改变时，重新查询加载维修项目可选项---js调用 */
  $scope.getProjectList = function () {
    return new Promise(function (resolve, reject) {
      if ($scope.service.brand && $scope.service.category && $scope.service.property
        && $scope.service.brand != '' && $scope.service.category != '' && $scope.service.property != '') {
        var sql = "select a.project,b.value from " + $schema.TABLE_NAME_SERVICEFEE + " a," + $schema.TABLE_NAME_METADATA +
          " b where a.project = b.key and a.brand_id = '" + $scope.service.brand + "' and a.category_id = '" + $scope.service.category +
          "' and a.property = '" + $scope.service.property + "' and b.context like 'SERVICE.PROJECT'";
        $mysqlite.select(sql).then(function (res) {
          if (res)
            $scope.projectList = res.data;
          resolve();
        }, function (e) {
          reject(e);
        });
      } else
        resolve();
    });
  };
  /** 查询维修费和路费：由品类、品牌、维修性质、维修项目四项决定（查询serviceFee表）---js调用*/
  $scope.getFee = function () {
    return new Promise(function (resolve, reject) {
      if ($scope.service.brand && $scope.service.category && $scope.service.property && $scope.service.project
        && $scope.service.brand != '' && $scope.service.category != '' && $scope.service.project != '') {
        $mysqlite.selectOne('select * from ' + $schema.TABLE_NAME_SERVICEFEE + " where category_id = '" + $scope.service.category +
          "' and brand_id = '" + $scope.service.brand + "' and property = '" + $scope.service.property +
          "' and project = '" + $scope.service.project + "'").then(function (res) {
            if (res) {
              $scope.service.fee = res.fee;
              $scope.service.road_fee = res.road_fee;
            }
            resolve();
          }, function (e) {
            reject(e);
          });
      } else
        resolve();
    });
  };

  //赋值“到达时间”assigned_at
  $scope.setAssignedAt = function () {
    $scope.service.assigned_at = moment().format('YYYY-MM-DD HH:mm:ss');
  };
  //工单信息、产品信息、业务信息、配件信息 这几块的隐藏与显示控制
  $scope.flagChange = function (type) {
  
    $scope.flags = { orderFlag: false, goodsFlag: false, stateFlag: false, itemFlag: false };
    
    if (type == 'o')
      $scope.flags.orderFlag = true;
    else if (type == 'g')
      $scope.flags.goodsFlag = true;
    else if (type == 's')
      $scope.flags.stateFlag = true;
    else if (type == 'i')
      $scope.flags.itemFlag = true;
    //滚动到顶部
    $ionicScrollDelegate.scrollTop();
  };

  //缺件申请弹出层“确定”的回调函数
  $scope.itemCallback = function(obj){
    $scope.flags = { orderFlag: false, goodsFlag: false, stateFlag: false, itemFlag: true };
    $scope.service.state = "B";
    $scope.service.subState = "D";
    $scope.service.subState_value = '缺件待修';

    $scope.modifyFlag = {modify: undefined, order: undefined, item: true, finish: undefined, submit: undefined};
    $scope.item.selected = obj.selected;
    $scope.service.remark = obj.remark;
    $scope.item.remark = obj.remark;
    $scope.ok(1);
  };
  //完成弹出层“确定”的回调函数（弹出层中点“确定”先不提交服务端 先打开图片上传弹出层，图片弹出层中的操作按钮负责提交保存单据）
  $scope.completeCallback = function(obj){
    $scope.service = obj;
    //完成时间
    if($scope.service.complete_at_date && $scope.service.complete_at_time)
      $scope.service.complete_at = $scope.service.complete_at_date + ' ' + $scope.service.complete_at_time;
    else if($scope.service.complete_at_date)
      $scope.service.complete_at = $scope.service.complete_at_date;
    $scope.flags = { orderFlag: false, goodsFlag: false, stateFlag: true, itemFlag: false };
    $scope.service.state = "C";
    $scope.service.subState = "G";
    $scope.service.subState_value = '已完工';
    $scope.modifyFlag = {modify: undefined, order: undefined, item: undefined, finish: true, submit: undefined};

    //弹出图片选择层
    $scope.oldImages = $scope.service.attachments;
    $scope.imageModal = true;
    $scope.callbackOption = $scope.imageCallback;
    $ionicModal.fromTemplateUrl('views/service/images_select.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.select_modal = modal;
        $scope.select_modal.show();
    });
  };
  //图片选择弹出层中的按钮回调函数
  $scope.imageCallback = function(obj){
    $scope.select_modal.hide();
    $scope.newImages = obj;
    $scope.ok(1);
  };
  /**2修改。1改约。3提交。itemCallback(obj)缺件申请。完成completeCallback(obj) ---弹出需要填写的信息*/
  $scope.modifyFlag = {modify: undefined, order: undefined, item: undefined, finish: undefined, submit: undefined};
  $scope.modify = function(type){
    if(type == 2){//修改
      $scope.service.state == "B"
      $scope.service.subState = "C";
      $scope.modifyFlag = {modify: true, order: undefined, item: undefined, finish: undefined, submit: undefined};
      $scope.ok(2);
    }else if(type == 3){//提交
      var s = $filter('filter')($scope.metadatas, { context: 'SERVICE.SUB_STATE', app_state: 'Y', key: $scope.service.subState});
      if(s && s.length > 0)
        $scope.service.state = s[0].belong;
      $scope.modifyFlag = {modify: undefined, order: undefined, item: undefined, finish: undefined, submit: true};
      if($scope.service.state == "B")
        $scope.ok(2);
      else
        $scope.ok(1);
    } else if(type == 1){//改约--选择预约时间、设置工单状态
      $scope.mod_order_at = undefined;
      var ipObj1 = {
          callback: function (val) {  //设置预约日期
              $scope.mod_order_at = moment(val).format('YYYY-MM-DD');
              var ipOb2 = {
                callback: function (val) {//设置预约时间
                    if (typeof (val) === 'undefined') {
                        console.log('Time not selected');
                    } else {
                        $scope.service.order_at_date = $scope.mod_order_at;
                        $scope.flags = { orderFlag: true, goodsFlag: false, stateFlag: false, itemFlag: false };
                        $scope.service.state = "A";
                        $scope.service.subState = "B";
                        $scope.service.subState_value = '改约';
                        $scope.modifyFlag = {modify: undefined, order: true, item: undefined, finish: undefined, submit: undefined};

                        var selectedTime = new Date(val * 1000);
                        $scope.service.order_at_time = selectedTime.getUTCHours() + ':' + 
                            (selectedTime.getUTCMinutes()>9?selectedTime.getUTCMinutes():'0'+selectedTime.getUTCMinutes());
                        $scope.ok(1);
                    }
                }
              };
              ionicTimePicker.openTimePicker(ipOb2);//打开预约时间选择器
          }
      };
      if ($scope.before)
          ipObj1.to = new Date();
      if ($scope.after)
          ipObj1.from = new Date();
      ionicDatePicker.openDatePicker(ipObj1);//打开预约日期选择器
    }

  };

  //提交
  $scope.ok = function (type) {
    $ionicLoading.show({ template: '数据提交中...' });
    $scope.params = {
      tid: UserService.user.tid,
      uid: UserService.user.login_account
    };

    if($scope.service.model2 == '' && $scope.service.model_value)//员工之前填写的型号不在下拉列表的情况
      $scope.service.model = $scope.service.model_value;
    else if($scope.modeList && $scope.modeList.length > 0 )
      $scope.service.model = $scope.service.model2;

    $scope.service.fact_fee_new = $scope.service.fact_fee;
    if(!$scope.service.fact_fee_new)
      $scope.service.fact_fee_new = 0;
    //预约时间
    if($scope.service.order_at_date && $scope.service.order_at_time)
      $scope.service.order_at = $scope.service.order_at_date + ' ' + $scope.service.order_at_time;
    else if($scope.service.order_at_date)
      $scope.service.order_at = $scope.service.order_at_date;
    //派单时间
    if($scope.service.dispatch_at_date && $scope.service.dispatch_at_time)
      $scope.service.dispatch_at = $scope.service.dispatch_at_date + ' ' + $scope.service.dispatch_at_time;
    else if($scope.service.dispatch_at_date)
      $scope.service.dispatch_at = $scope.service.dispatch_at_date;

    $scope.service.repair_at = moment().format('YYYY-MM-DD HH:mm:ss');

    $scope.service.assignInfo = '';//配件

    return new Promise.resolve().then(function () {
      $scope.service.itemSelected = $scope.item.selected;
      return $scope.item.selected;
    }).each(function (item) {
      //id@价格@数量@名字@实虚核@保内外@cost成本价
      if (item.item_id) {
        var newitem = item.item_id + '@' + item.price + '@' + item.count + '@' + item.name + '@' + item.property + '@' + item.sellchannel + '@' + item.cost;
        if($scope.service.assignInfo && $scope.service.assignInfo != '')
          $scope.service.assignInfo = $scope.service.assignInfo + ',' + newitem;
        else
          $scope.service.assignInfo = newitem;
        $scope.oldItems.push({
            item_id: item.item_id, name: item.name, model: item.model, code: item.code, property: item.property, 
            cost: item.cost, price: item.price, count: item.count, sellchannel: item.sellchannel });
      }
    }).then(function () {
      $scope.service.newImages = $scope.newImages;
      $scope.service.oldImages = $scope.service.attachments;
      return $scope.newImages;
    }).each(function (im) {
      if ($scope.service.attachments)
        $scope.service.attachments.push({ name: im.name });
      else
        $scope.service.attachments = [{ name: im.name }];
    }).then(function () {
      $scope.service.deal_staff = UserService.user._id;
      $scope.service.lat = CommonService.LOCATION_LAT;
      $scope.service.lng = CommonService.LOCATION_LNG;
      /**
      return $mysqlite.selectOne('select * from ' + $schema.TABLE_NAME_FAIL_SERVICE + " where service_id = '" + $scope.service.service_id + "'");
    }).then(function (row) {//保存到本地数据库
      if (!row || !row.service_id){
        var fsl = $mysqlite.tableToParams($schema.T_STRUCTURE_FAIL_SERVICE, $scope.service);
        return $mysqlite.insert($schema.TABLE_NAME_FAIL_SERVICE, fsl);
      }else
        return $mysqlite.update($schema.TABLE_NAME_FAIL_SERVICE, $scope.service, " where service_id = '" + $scope.service.service_id + "'");
    }).then(function () { */
      if ($cordovaNetwork.isOnline()) {
        return new Promise.resolve().then(function () {
          $scope.params.service = $scope.service;
          return $myhttp.put(CommonService.URL_BASE + CommonService.URL_MODIFY_SERVICE, $scope.params);
        }).then(function () {
          return $mysqlite.delete($schema.TABLE_NAME_FAIL_SERVICE, " where service_id = '" + $scope.service.service_id + "'");
        }).then(function () {

        });
      }
    }).then(function (r) {
      $ionicLoading.hide();
      return CommonService.showAlert('单据保存成功');
    }).then(function () {
      if(type=='2'){//修改 继续停留在车页面
        $scope.newImages = [];
        $scope.item.selected = [];
        $scope.flags = { orderFlag: true, goodsFlag: false, stateFlag: false, itemFlag: false };
      }else
        $state.go('app.service.list', { state: 'B' });
    }).catch(function (err) {
      $ionicLoading.hide();
      CommonService.showAlert('单据保存失败err:' + JSON.stringify(err));
    });

  }

});

app.controller('ServiceFailListCtrl', function ($scope, $stateParams, $state, $myhttp, CommonService, $mysqlite) {
  

});
