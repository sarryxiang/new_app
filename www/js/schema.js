/**
 * 手机数据库 表名 表结构（表名 表字段尽量与服务端对应）
 */
var app = angular.module('app.schema', []);

app.factory('$schema', function ($rootScope) {
    var service = {};
    service.DATEBASE_NAME = "wxtefu";

    service.DATEBASE_VERSION = 5;

    //配置表
    service.TABLE_NAME_CONFIG = "ip_config";
    service.T_STRUCTURE_CONFIG = {
        model: service.TABLE_NAME_CONFIG,
        schema: {
            id: "integer",
            ip: "text",
            port: "text",
            project_name: "text",
            staff_id: "text",//_id 推送别名
            wcode: "text",
            login_account: "text",//name or wcode
            password: "text",
            station: "text",//部门 推送标签
            tenant_id: "text",
            tid: "text",
            tags: "text",//标签
            alias: "text",//别名
            rf_count: "text",//工单数量
            version: "text",//数据库版本
            mBtName: "text",
            mBtAddress: "text",
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text"
        }
    };

    //工单表
    service.TABLE_NAME_SERVICE = "service";
    //type上门,门市,销售,安装		dispatch_at派单时间	order_at预约时间	repair_percentage维修比例 		road_percentage上门比例	item_percentage配件比例
    service.T_STRUCTURE_SERVICE = {
        model: service.TABLE_NAME_SERVICE,
        schema: {
            _id: "text",
            service_id: "text",//维修单号
            call_session_id: "text",
            call_caller: "text",//来电电话
            type: "text",
            state: "text",//状态
            subState: "text",//子状态
            brand: "text",//产品类型
            category: "text",//产品品牌
            user_name: "text",//客户姓名
            user_tel: "text",//客户电话
            user_province: "text",
            user_city: "text",
            user_area: "text",
            user_community: "text",
            user_address: "text",//客户地址
            content: "text",//问题/故障描述
            result: "text",//描述
            result_type: "text",//处理结果
            user: "text",
            customer: "text",
            station: "text",//部门
            deal_user: "text",
            create_at: "text",//受理时间
            complete_at: "text",
            callback_at: "text",
            callback_result: "text",
            callback_staff: "text",
            callback_phone: "text",//回访号码
            dispatch_at: "text",//派发时间
            order_at: "text",//预约时间
            model: "text",//产品型号
            buy_at: "text",//购买日期
            production_code: "text",//生产批号
            policy: "text",
            receive_at: "text",
            shop: "text",//购买商店
            satisfy: "text",
            remark: "text",//备注
            source: "text",
            fee: "text",//维修服务费维修费
            road_fee: "text",//上门服务费路费
            event: "text",
            cancel_type: "text",
            repair_type: "text",
            appearance: "text",//故障现象
            attachment: "text",
            process: "text",//完成度
            property: "text",//维修性质
            project: "text",//维修项目
            user_type: "text",
            last_repair_at: "text",
            repair_at: "text",//修复时间
            fault_reason: "text",//故障原因
            treatment_method: "text",
            invoice: "text",//发票号
            in_store: "text",
            user_remark: "text",//客户信息处的备注
            documentation: "text",
            event_type: "text",
            factory_no: "text",
            fact_fee: "text",//合计 价格
            reason: "text",
            keep_item: "text",
            qr_code: "text",//出厂编码
            position: "text",
            deal_staff: "text",
            repair_percentage: "text",
            repair_fixed: "text",
            road_percentage: "text",
            road_fixed: "text",
            item_percentage: "text",
            item_fixed: "text",
            other_fee: "text",
            financial_fee: "text",
            assigned_at: "text",
            lat: "text",
            lng: "text",
            attachments: "text",//图片
            //下面五个扩展字段
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text",
            ////显示用
            old_items: 'text'
        }
    };

    //未提交成功表 appearance故障现象  process完成度
    service.TABLE_NAME_FAIL_SERVICE = "fail_service";
    service.T_STRUCTURE_FAIL_SERVICE = {
        model: service.TABLE_NAME_FAIL_SERVICE,
        schema: {
            _id: "text",
            service_id: "text",
            order_at: "text",
            brand: "text",
            model: "text",
            property: "text",
            state: "text",
            subState: "text",
            shop: "text",
            invoice: "text",
            remark: "text",
            result: "text",
            buy_at: "text",
            category: "text",
            result_type: "text",
            callback_phone: "text",
            production_code: "text",
            qr_code: "text",
            repair_at: "text",
            user_name: "text",
            appearance: "text",
            process: "text",
            //下面两个显示使用
            create_at: "text",
            dispatch_at: "text",
            fee: "text",
            road_fee: "text",
            fact_fee: "text",
            project: "text",
            fault_reason: "text",//content2
            deal_staff: "text",
            assigned_at: "text",
            lng: "text",
            lat: "text",
            user_address: "text",
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text",

            attachments: "text",//上传的附件信息
            assignInfo: "text",//选择配件信息

            //显示用
            itemSelected: "text",
            newImages: "text",
            oldImages: "text",
            old_items: 'text'
        }
    };

    //配件表 propertyA耗材 B电器件 brand品牌  type品类
    service.TABLE_NAME_ITEM = "item";
    //code厂家编码		sizes规格		currency通用性
    service.T_STRUCTURE_ITEM = {
        model: service.TABLE_NAME_ITEM,
        schema: {
            _id: "text",
            id: "integer",
            item_id: "text",
            name: "text",
            model: "text",
            intro: "text",
            brand: "text",
            category: "text",
            property: "text",
            state: "text",
            price: "text",
            cost: "text",
            create_at: "text",
            update_at: "text",
            expire: "text",
            code: "text",
            sizes: "text",
            currency: "text",
            into_price: "text",
            take_price: "text",
            allot_price: "text",
            last_into: "text",
            last_cost: "text",
            remark: "text",
            sync_id: "text",
            private: "text",
            //下面五个扩展字段，未使用
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text"
        }
    };

    //产品表metadata
    service.TABLE_NAME_METADATA = "metadata";
    //context:产品类型MACHINE.TYPE;产品品牌MACHINE.BRAND;维修项目SERVICE.PROJECT；2015-06-24完成度SERVICE.PROCESS;2016-08-10维修性质SERVICE.SELLCHANNEL
    service.T_STRUCTURE_METADATA = {
        model: service.TABLE_NAME_METADATA,
        schema: {
            _id: "text",
            context: "text",
            key: "text",
            value: "text",
            state: "text",
            belong: "text",
            transform: "text",
            process: "text",
            remark: "text",
            sync_id: "text",
            private: "text",
            app_state: "text",//手机端显示的可选状态，context='SERVICE.SUB_STATE'
            //下面五个扩展字段，未使用
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text"
        }
    };

    //产品品牌、类型关联表
    service.TABLE_NAME_SERVICEFEE = "serviceFee";
    service.T_STRUCTURE_SERVICEFEE = {
        model: service.TABLE_NAME_SERVICEFEE,
        schema: {
            _id: "text",
            brand_id: "text",
            category_id: "text",
            project: "text",
            property: "text",
            fee: "text",
            road_fee: "text",
            sync_id: "text",
            private: "text",
            //下面五个扩展字段，未使用
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text"
        }
    };
    //产品品牌、类型关联表 型号 brand_category
    service.TABLE_NAME_BRAND_CATEGORY = "brand_category";
    service.T_STRUCTURE_BRAND_CATEGORY = {
        model: service.TABLE_NAME_BRAND_CATEGORY,
        schema: {
            _id: "text",
            brand_id: "text",
            category_id: "text",
            projects: "text",
            properties: "text",
            state: "text",
            sync_id: "text",
            private: "text",
            //下面五个扩展字段，未使用
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text"
        }
    };

    //部门表(对应efu5表station)，打印使用name名称 emai地址	remark监督电话	conTel服务中心电话
    service.TABLE_NAME_DEPARTMENT = "department";
    service.T_STRUCTURE_DEPARTMENT = {
        model: service.TABLE_NAME_DEPARTMENT,
        schema: {
            _id: "text",
            id: "integer",
            name: "text",
            ancestors: "text",
            parent: "text",
            tel: "text",
            province: "text",
            city: "text",
            area: "text",
            address: "text",
            state: "text",
            remark: "text",
            //下面五个扩展字段，未使用
            extend1: "text",
            extend2: "text",
            extend3: "text",
            extend4: "text",
            extend5: "text"
        }
    };

    /**
     * 数据更新时间表
     * name更新的东西:R工单，P配件，C产品(C产品品牌型号关联表、C3品牌、C1品类、C2品类扩展字段),H历史配件，D基础数据(D部门、W完成度、W0维修项目)
     * value更新的时间time
     */
    service.TABLE_NAME_UPDATE_TIME = "update_time";
    service.T_STRUCTURE_UPDATE_TIME = {
        model: service.TABLE_NAME_UPDATE_TIME,
        schema: {
            name: "text",
            value: "text"
        }
    };

    /**
     * 品类扩展字段表--对应efu5表setting中字段extend_setting的值
     * category品类
     * view扩展字段在工单页面中显示名字
     * val扩展字段在工单的字段名
     * scan是否需要扫描获取数据 Y是
     */
    service.TABLE_NAME_SETTING = "setting";
    service.T_STRUCTURE_SETTING = {
        model: service.TABLE_NAME_SETTING,
        schema: {
            _id: "text",
            category: "text",
            view: "text",
            val: "text",
            scan: "text"
        }
    };



    return service;
});
