angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    /*.state('tabsController.cameraTabDefaultPage', {
    url: '/page2',
    views: {
      'tab1': {
        templateUrl: 'templates/cameraTabDefaultPage.html',
        controller: 'cameraTabDefaultPageCtrl'
      }
    }
  })
  .state('tabsController.cartTabDefaultPage', {
    url: '/page3',
    views: {
      'tab2': {
        templateUrl: 'templates/cartTabDefaultPage.html',
        controller: 'cartTabDefaultPageCtrl'
      }
    }
  })
  .state('tabsController.cloudTabDefaultPage', {
    url: '/page4',
    views: {
      'tab3': {
        templateUrl: 'templates/cloudTabDefaultPage.html',
        controller: 'cloudTabDefaultPageCtrl'
      }
    }
  })
  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })*/
    .state('welcome', {
      url: '/welcome',
      templateUrl: 'views/welcome.html'
    })
  .state('login', {
      cache: false,
      url: '/login',
      templateUrl: 'views/login.html'
    }).state('app', {
      cache: false,
      url: '/app',
      templateUrl: 'views/menu.html',
      abstract:true
    }).state('app.service', {
      url: '/service',
      templateUrl: 'views/service/service-tabs.html',
      abstract:true
    }).state('app.service.list', {
      cache: false,
      url: '/list/:state',
      views: {
        'tab1': {
          templateUrl: 'views/service/list.html'
        }
      }
    }).state('app.service.list3', {//未预约
      cache: false,
      url: '/list3/:state',
      views: {
        'tab3': {
          templateUrl: 'views/service/list.html'
        }
      }
    }).state('app.service.list4', {//预约时间-今天
      cache: false,
      url: '/list4/:state',
      views: {
        'tab4': {
          templateUrl: 'views/service/list.html'
        }
      }
    }).state('app.service.modify', {
      cache: false,
      url: '/modify/:_id',
      views: {
        'tab1': {
          templateUrl: 'views/service/modify.html'
        }
      }
    }).state('app.service.fail-list', {
      url: '/fail-list',
      views: {
        'tab2': {
          templateUrl: 'views/service/fail-list.html'
        }
      }
    }).state('app.service.fail-detail', {
      url: '/fail-detail/:_id',
      views: {
        'tab2': {
          templateUrl: 'views/service/fail-detail.html'
        }
      }
    })
    $urlRouterProvider.otherwise('/welcome')
  });
