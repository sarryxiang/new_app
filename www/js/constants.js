var app = angular.module('app.constants', []);

app.factory('CommonService', function ($rootScope, $ionicPopup, $timeout) {
  var service = {};

  /**============一些带默认值得常量参数=========== */
  service.JPUSH_TAGS = "";//是否设置成功推送用的标签别名
  service.JPUSH_ALIAS = "";

  service.LOGIN_ACCOUNT = "管理员";
  service.PASSWORD = "123456";
  service.TENANT_ID = "CS10001000";//01016000SH
  
  service.SERVER_IP = "www.efu5.com";//"182.92.165.224";//"";www.efu5.com
  service.SERVER_PORT = "80";//8080";//"80";
  //service.URL_BASE = "http://" + service.SERVER_IP + ":" + service.SERVER_PORT + "/interface";
  service.URL_BASE = "http://" + service.SERVER_IP + ":" + service.SERVER_PORT + "/mobile_interface";
  service.URL_GET_TID = "http://www.efu5.com:3000/interface/3328e4f7fbcce95180abf8bc6075e78b/outlet/";
  //版本
  service.APP_VERSION = '4.1.8';
  service.URL_UPDATE_VERJSON = "http://" + service.SERVER_IP + ":" + service.SERVER_PORT + '/views/myapk/appversion.json';
  service.URL_APK = "http://" + service.SERVER_IP + ":" + service.SERVER_PORT + '/views/myapk/chinaefu5.apk';
  service.URL_IPA = 'https://itunes.apple.com/us/app/e服务-官方版/id1234068513?l=zh&ls=1&mt=8';
  /** 蓝牙打印机名字*/
  service.MBT_NAME = null;
  /**蓝牙打印机连接地址*/
  service.MBT_ADDRESS = null;
  service.PAGE_SIZE = 50;
  service.LOCATION_LAT;
  service.LOCATION_LNG;

  //维修状态
  // service.STATE_LIST = [{ key: 'C', value: '已派单' }, { key: 'B', value: '改约' }, { key: 'D', value: '缺件待修' },
  //   { key: 'E', value: '到件需修' }, { key: 'G', value: '已修复' }, { key: 'X', value: '工单取消' }];
  //处理结果
  service.RESULT_LIST = [{ key: 'A', value: '修复' }, { key: 'B', value: '申购配件' }, { key: 'C', value: '放弃维修' }];

  /**======end======一些带默认值得常量参数=========== */

  /**=========访问服务端的URL======== */
  /**登录 */
  service.URL_LOGIN = '/loginByMobile';
  /**登出 */
  service.URL_LOGOUT = '/exitByMobile';
  /**获取工单 */
  service.URL_GET_SERVICELIST = "/service/getService4Mobile";
  /**获取
   * 产品类型MACHINE.TYPE;产品品牌MACHINE.BRAND;维修项目SERVICE.PROJECT；
   * 2015-06-24完成度SERVICE.PROCESS;
   * 2016-08-10维修性质SERVICE.SELLCHANNEL
   * context=*/
  service.URL_GET_METADATA = "/service/getMetadateInfo";
  /**获取登录员工部门 */
  service.URL_GET_DEPARTMENT = "/service/getUpdateWorkArea";
  /**获取产品品类扩展字段 */
  service.URL_GET_SETTING = "/service/getSetting";
  /**获取产品品牌型号关联表 */
  service.URL_GET_SERVICEFEE = "/service/getUpdateRelation";
  /**获取产品品牌型号关联表--brand_category */
  service.URL_GET_BRAND_CATEGORY = "/service/getBrandCategory";
  /**获取配件 */
  service.URL_GET_ITEMS = "/service/getUpdateGoodsInfo";
  /**修改工单 */
  service.URL_MODIFY_SERVICE = "/service/submitServiceResult";
  /** 定位手机经纬度到服务端 */
  //service.URL_SET_LOCATION = "/locUpdate.action";
  service.URL_SET_LOCATION = "/service/postMobileLocation";
  /**上传图片路径 */
  service.URL_UPLOAD_PIC = "/upload/service";
  /**======end===访问服务端的URL======== */




  /**=========公用方法============ */
  // 触发一个按钮点击，或一些其他目标
  service.showPopup = function () {
    $rootScope.data = {}
    // 一个精心制作的自定义弹窗
    var myPopup = $ionicPopup.show({
      template: '<input type="password" ng-model="data.wifi">',
      title: 'Enter Wi-Fi Password',
      subTitle: 'Please use normal things',
      scope: $rootScope,
      buttons: [
        { text: '取消' },
        {
          text: '<b>保存</b>',
          type: 'button-positive',
          onTap: function (e) {
            if (!$rootScope.data.wifi) {
              //不允许用户关闭，除非他键入wifi密码
              e.preventDefault();
            } else {
              return $rootScope.data.wifi;
            }
          }
        },
      ]
    });
    return myPopup;
    /**
    myPopup.then(function (res) {//res 输入的值
      console.log('Tapped!--', res);
    });
    $timeout(function () {
      myPopup.close(); //由于某种原因3秒后关闭弹出
    }, 3000);
     */
  };
  // 一个确认对话框
  service.showConfirm = function (tips) {
    var confirmPopup = $ionicPopup.confirm({
      title: '确认提示',
      template: tips, 
      cancelText: '取消',
      okText: '确定'
    });
    return confirmPopup;
    /**
    confirmPopup.then(function (res) {
      if (res) {
        console.log('You are sure');
      } else {
        console.log('You are not sure');
      }
    }); */
  };
  // 一个提示对话框
  service.showAlert = function (mes) {
    var alertPopup = $ionicPopup.alert({
      title: '消息提醒',
      template: mes
    });
    return alertPopup;
    /**
    alertPopup.then(function (res) {
      console.log('Thank you for not eating my delicious ice cream cone');
    });
     */
  };
  /**====end=====公用方法============ */

  return service;
});
