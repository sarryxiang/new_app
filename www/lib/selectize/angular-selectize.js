(function () {
    angular.module('selectize', []);

    angular.module('selectize').directive('selectize', function ($timeout) {
        return {
            // Restrict it to be an attribute in this case
            restrict: 'A',
            scope: {
                options: '=',
                ngModel: '='
            },
            // responsible for registering DOM listeners as well as updating the DOM
            link: function (scope, element, attrs) {
                
                scope.$watch('options', function () {
                    if (scope.options) {
                        $timeout(function () {
                            $select = $(element).selectize(scope.$eval(attrs.selectize));
                            selectElm = $select[0].selectize;
                            for (var i = 0; i < scope.options.length; i++) {
                                selectElm.addOption(scope.options[i]);
                            }
                            selectElm.setValue(scope.ngModel, true);                          
                        })
                    }
                })
            }
        };
    });

}).call(this);